﻿namespace QLDSV_CSDLPT.BaoCao
{
    partial class rpFormDSThiHetMon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.qLDSVDataSet = new QLDSV_CSDLPT.QLDSVDataSet();
            this.v_DSPMBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.v_DSPMTableAdapter = new QLDSV_CSDLPT.QLDSVDataSetTableAdapters.V_DSPMTableAdapter();
            this.tableAdapterManager = new QLDSV_CSDLPT.QLDSVDataSetTableAdapters.TableAdapterManager();
            this.v_DSPMComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.qLDSVDataSetMain = new QLDSV_CSDLPT.QLDSVDataSetMain();
            this.lOPBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lOPTableAdapter = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.LOPTableAdapter();
            this.tableAdapterManager1 = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.TableAdapterManager();
            this.mONHOCTableAdapter = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.MONHOCTableAdapter();
            this.lOPComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.mONHOCBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mONHOCComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.hOCKYComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lANTHIComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.nGAYTHIDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_DSPMBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSetMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lOPBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mONHOCBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // qLDSVDataSet
            // 
            this.qLDSVDataSet.DataSetName = "QLDSVDataSet";
            this.qLDSVDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // v_DSPMBindingSource
            // 
            this.v_DSPMBindingSource.DataMember = "V_DSPM";
            this.v_DSPMBindingSource.DataSource = this.qLDSVDataSet;
            // 
            // v_DSPMTableAdapter
            // 
            this.v_DSPMTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = QLDSV_CSDLPT.QLDSVDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // v_DSPMComboBox
            // 
            this.v_DSPMComboBox.BackColor = System.Drawing.Color.White;
            this.v_DSPMComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.v_DSPMComboBox.FormattingEnabled = true;
            this.v_DSPMComboBox.Location = new System.Drawing.Point(105, 29);
            this.v_DSPMComboBox.Name = "v_DSPMComboBox";
            this.v_DSPMComboBox.Size = new System.Drawing.Size(300, 21);
            this.v_DSPMComboBox.TabIndex = 1;
            this.v_DSPMComboBox.SelectedIndexChanged += new System.EventHandler(this.v_DSPMComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "KHOA: ";
            // 
            // qLDSVDataSetMain
            // 
            this.qLDSVDataSetMain.DataSetName = "QLDSVDataSetMain";
            this.qLDSVDataSetMain.EnforceConstraints = false;
            this.qLDSVDataSetMain.Locale = new System.Globalization.CultureInfo("en-US");
            this.qLDSVDataSetMain.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lOPBindingSource
            // 
            this.lOPBindingSource.DataMember = "LOP";
            this.lOPBindingSource.DataSource = this.qLDSVDataSetMain;
            // 
            // lOPTableAdapter
            // 
            this.lOPTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.DIEMTableAdapter = null;
            this.tableAdapterManager1.GIANGVIENTableAdapter = null;
            this.tableAdapterManager1.HOCPHITableAdapter = null;
            this.tableAdapterManager1.KHOATableAdapter = null;
            this.tableAdapterManager1.LOPTableAdapter = this.lOPTableAdapter;
            this.tableAdapterManager1.MONHOCTableAdapter = this.mONHOCTableAdapter;
            this.tableAdapterManager1.SINHVIENTableAdapter = null;
            this.tableAdapterManager1.UpdateOrder = QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // mONHOCTableAdapter
            // 
            this.mONHOCTableAdapter.ClearBeforeFill = true;
            // 
            // lOPComboBox
            // 
            this.lOPComboBox.DataSource = this.lOPBindingSource;
            this.lOPComboBox.DisplayMember = "TENLOP";
            this.lOPComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lOPComboBox.FormattingEnabled = true;
            this.lOPComboBox.Location = new System.Drawing.Point(105, 69);
            this.lOPComboBox.Name = "lOPComboBox";
            this.lOPComboBox.Size = new System.Drawing.Size(300, 21);
            this.lOPComboBox.TabIndex = 2;
            this.lOPComboBox.ValueMember = "TENLOP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "LỚP: ";
            // 
            // mONHOCBindingSource
            // 
            this.mONHOCBindingSource.DataMember = "MONHOC";
            this.mONHOCBindingSource.DataSource = this.qLDSVDataSetMain;
            // 
            // mONHOCComboBox
            // 
            this.mONHOCComboBox.DataSource = this.mONHOCBindingSource;
            this.mONHOCComboBox.DisplayMember = "TENMH";
            this.mONHOCComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mONHOCComboBox.FormattingEnabled = true;
            this.mONHOCComboBox.Location = new System.Drawing.Point(105, 96);
            this.mONHOCComboBox.Name = "mONHOCComboBox";
            this.mONHOCComboBox.Size = new System.Drawing.Size(300, 21);
            this.mONHOCComboBox.TabIndex = 4;
            this.mONHOCComboBox.ValueMember = "MAMH";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "MÔN: ";
            // 
            // hOCKYComboBox
            // 
            this.hOCKYComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hOCKYComboBox.FormattingEnabled = true;
            this.hOCKYComboBox.Location = new System.Drawing.Point(105, 123);
            this.hOCKYComboBox.Name = "hOCKYComboBox";
            this.hOCKYComboBox.Size = new System.Drawing.Size(76, 21);
            this.hOCKYComboBox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "MÔN: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "HỌC KỲ:";
            // 
            // lANTHIComboBox
            // 
            this.lANTHIComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lANTHIComboBox.FormattingEnabled = true;
            this.lANTHIComboBox.Location = new System.Drawing.Point(326, 123);
            this.lANTHIComboBox.Name = "lANTHIComboBox";
            this.lANTHIComboBox.Size = new System.Drawing.Size(79, 21);
            this.lANTHIComboBox.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(270, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "LẦN THI:";
            // 
            // nGAYTHIDateTimePicker
            // 
            this.nGAYTHIDateTimePicker.Location = new System.Drawing.Point(105, 150);
            this.nGAYTHIDateTimePicker.Name = "nGAYTHIDateTimePicker";
            this.nGAYTHIDateTimePicker.Size = new System.Drawing.Size(300, 20);
            this.nGAYTHIDateTimePicker.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(46, 154);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "NGÀY THI:";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(212, 186);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // rpFormDSThiHetMon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.nGAYTHIDateTimePicker);
            this.Controls.Add(this.lANTHIComboBox);
            this.Controls.Add(this.hOCKYComboBox);
            this.Controls.Add(this.mONHOCComboBox);
            this.Controls.Add(this.lOPComboBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.v_DSPMComboBox);
            this.Name = "rpFormDSThiHetMon";
            this.Text = "rpFormDSThiHetMon";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.rpFormDSThiHetMon_FormClosing);
            this.Load += new System.EventHandler(this.rpFormDSThiHetMon_Load);
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_DSPMBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSetMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lOPBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mONHOCBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private QLDSVDataSet qLDSVDataSet;
        private System.Windows.Forms.BindingSource v_DSPMBindingSource;
        private QLDSVDataSetTableAdapters.V_DSPMTableAdapter v_DSPMTableAdapter;
        private QLDSVDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.ComboBox v_DSPMComboBox;
        private System.Windows.Forms.Label label1;
        private QLDSVDataSetMain qLDSVDataSetMain;
        private System.Windows.Forms.BindingSource lOPBindingSource;
        private QLDSVDataSetMainTableAdapters.LOPTableAdapter lOPTableAdapter;
        private QLDSVDataSetMainTableAdapters.TableAdapterManager tableAdapterManager1;
        private QLDSVDataSetMainTableAdapters.MONHOCTableAdapter mONHOCTableAdapter;
        private System.Windows.Forms.ComboBox lOPComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource mONHOCBindingSource;
        private System.Windows.Forms.ComboBox mONHOCComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox hOCKYComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox lANTHIComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker nGAYTHIDateTimePicker;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnOK;
    }
}