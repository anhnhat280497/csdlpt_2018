﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QLDSV_CSDLPT.BaoCao
{
    public partial class RpDSDongHP : DevExpress.XtraReports.UI.XtraReport
    {
        public RpDSDongHP(string lop, string nienkhoa, int hocky)
        {
            InitializeComponent();
            //this.DataSource = qldsvDataSetMain1.SP_DSHOCPHICUALOP;
            this.sP_DSHOCPHICUALOPTableAdapter1.Connection.ConnectionString = Program.connectionString;
            this.sP_DSHOCPHICUALOPTableAdapter1.Fill(qldsvDataSetMain1.SP_DSHOCPHICUALOP, lop, nienkhoa, hocky);
            //this.sP_DSHOCPHICUALOPTableAdapter1.Fill(qldsvDataSetMain1.SP_DSHOCPHICUALOP, "d15cqcn1", "2015-2016", 1);
        }

    }
}
