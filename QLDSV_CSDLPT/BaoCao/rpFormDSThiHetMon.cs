﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLDSV_CSDLPT.BaoCao
{
    public partial class rpFormDSThiHetMon : Form
    {
        public rpFormDSThiHetMon()
        {
            InitializeComponent();
        }

        private void rpFormDSThiHetMon_Load(object sender, EventArgs e)
        {
            this.mONHOCTableAdapter.Connection.ConnectionString = Program.connectionString;
            this.lOPTableAdapter.Connection.ConnectionString = Program.connectionString;
            // TODO: This line of code loads data into the 'qLDSVDataSetMain.MONHOC' table. You can move, or remove it, as needed.
            this.mONHOCTableAdapter.Fill(this.qLDSVDataSetMain.MONHOC);
            // TODO: This line of code loads data into the 'qLDSVDataSetMain.LOP' table. You can move, or remove it, as needed.
            this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
            // TODO: This line of code loads data into the 'qLDSVDataSet.V_DSPM' table. You can move, or remove it, as needed.
            this.v_DSPMTableAdapter.Fill(this.qLDSVDataSet.V_DSPM);

            this.v_DSPMComboBox.DataSource = Program.bindingSourceKhoa;
            v_DSPMComboBox.DisplayMember = "description";
            v_DSPMComboBox.ValueMember = "subscriber_server";

            if (Program.groupHT == "USER")
            {
                v_DSPMComboBox.Enabled = false;
            }
            else
            {
                Program.bindingSourceKhoa.Filter = "description <> 'Quản lý học phí'";
            }

            IDictionary<int, string> dict = new Dictionary<int, string>();
            dict.Add(1, "1");
            dict.Add(2, "2");
            hOCKYComboBox.DataSource = new BindingSource(dict, null);
            hOCKYComboBox.DisplayMember = "Value";
            hOCKYComboBox.ValueMember = "Key";
            lANTHIComboBox.DataSource = new BindingSource(dict, null);
            lANTHIComboBox.DisplayMember = "Value";
            lANTHIComboBox.ValueMember = "Key";

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string lop = lOPComboBox.Text, mon = mONHOCComboBox.Text;
            int hocky = Int16.Parse(hOCKYComboBox.Text), lanthi = Int16.Parse(lANTHIComboBox.Text);
            DateTime ngaythi = Convert.ToDateTime(nGAYTHIDateTimePicker.Text);

            try
            {
                RpPhieuDiemThi rpt = new RpPhieuDiemThi(lop, mon, hocky, ngaythi, lanthi);
                if (rpt.xrLop.Text == "false")
                {
                    MessageBox.Show("Không có dữ liệu!");
                    return;
                }
                rpt.xrLop.Text = "LỚP: " + lop;
                rpt.xrMonHoc.Text += mon;
                rpt.xrNgayThi.Text += ngaythi.ToLongDateString();
                rpt.xrHocKy.Text += hocky;
                rpt.xrLanThi.Text += lanthi;
                ReportPrintTool print = new ReportPrintTool(rpt);
                print.ShowPreviewDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            //this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
        }

        private void v_DSPMComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.v_DSPMComboBox.ValueMember != "" && Program.formLogin.Visible == false)
            {
                if (Program.server != this.v_DSPMComboBox.SelectedValue.ToString())
                {
                    Program.server = this.v_DSPMComboBox.SelectedValue.ToString();
                    if (Program.userNameHTai != Program.remoteLogin)
                    {
                        Program.userNameHTai = Program.remoteLogin;
                        Program.passwordHTai = Program.remotePassword;
                    }
                    else
                    {
                        Program.userNameHTai = Program.userName;
                        Program.passwordHTai = Program.password;
                    }
                    Program.connectionString = "Server = " + Program.server + "; Database = " + Program.database + "; User Id = "
                        + Program.userNameHTai + "; Password = " + Program.passwordHTai;
                    Program.sqlConnection = new SqlConnection(Program.connectionString);
                    Program.sqlConnection.Open();
                    this.lOPTableAdapter.Connection.ConnectionString = Program.connectionString;
                    this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
                }
            }
        }

        private void rpFormDSThiHetMon_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.dSThiHetMon = null;
        }
    }
}
