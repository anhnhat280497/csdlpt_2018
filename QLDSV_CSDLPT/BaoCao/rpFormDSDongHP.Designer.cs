﻿namespace QLDSV_CSDLPT.BaoCao
{
    partial class rpFormDSDongHP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.qLDSVDataSet = new QLDSV_CSDLPT.QLDSVDataSet();
            this.tableAdapterManager = new QLDSV_CSDLPT.QLDSVDataSetTableAdapters.TableAdapterManager();
            this.qLDSVDataSetMain = new QLDSV_CSDLPT.QLDSVDataSetMain();
            this.lOPBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lOPTableAdapter = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.LOPTableAdapter();
            this.tableAdapterManager1 = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.TableAdapterManager();
            this.label1 = new System.Windows.Forms.Label();
            this.hOCKYComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.kHOABindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.kHOATableAdapter = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.KHOATableAdapter();
            this.kHOAComboBox = new System.Windows.Forms.ComboBox();
            this.lOPBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.lOPComboBox = new System.Windows.Forms.ComboBox();
            this.hOCPHIBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.hOCPHITableAdapter = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.HOCPHITableAdapter();
            this.hOCPHIComboBox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSetMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lOPBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kHOABindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lOPBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hOCPHIBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // qLDSVDataSet
            // 
            this.qLDSVDataSet.DataSetName = "QLDSVDataSet";
            this.qLDSVDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = QLDSV_CSDLPT.QLDSVDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // qLDSVDataSetMain
            // 
            this.qLDSVDataSetMain.DataSetName = "QLDSVDataSetMain";
            this.qLDSVDataSetMain.EnforceConstraints = false;
            this.qLDSVDataSetMain.Locale = new System.Globalization.CultureInfo("en-US");
            this.qLDSVDataSetMain.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lOPBindingSource
            // 
            this.lOPBindingSource.DataMember = "LOP";
            this.lOPBindingSource.DataSource = this.qLDSVDataSetMain;
            // 
            // lOPTableAdapter
            // 
            this.lOPTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.DIEMTableAdapter = null;
            this.tableAdapterManager1.GIANGVIENTableAdapter = null;
            this.tableAdapterManager1.HOCPHITableAdapter = null;
            this.tableAdapterManager1.KHOATableAdapter = null;
            this.tableAdapterManager1.LOPTableAdapter = this.lOPTableAdapter;
            this.tableAdapterManager1.MONHOCTableAdapter = null;
            this.tableAdapterManager1.SINHVIENTableAdapter = null;
            this.tableAdapterManager1.UpdateOrder = QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "KHOA:";
            // 
            // hOCKYComboBox
            // 
            this.hOCKYComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hOCKYComboBox.FormattingEnabled = true;
            this.hOCKYComboBox.Location = new System.Drawing.Point(102, 149);
            this.hOCKYComboBox.Name = "hOCKYComboBox";
            this.hOCKYComboBox.Size = new System.Drawing.Size(121, 21);
            this.hOCKYComboBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "LỚP:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "NIÊN KHÓA:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "HỌC KỲ:";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(217, 198);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // kHOABindingSource
            // 
            this.kHOABindingSource.DataMember = "KHOA";
            this.kHOABindingSource.DataSource = this.qLDSVDataSetMain;
            // 
            // kHOATableAdapter
            // 
            this.kHOATableAdapter.ClearBeforeFill = true;
            // 
            // kHOAComboBox
            // 
            this.kHOAComboBox.DataSource = this.kHOABindingSource;
            this.kHOAComboBox.DisplayMember = "TENKH";
            this.kHOAComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.kHOAComboBox.FormattingEnabled = true;
            this.kHOAComboBox.Location = new System.Drawing.Point(102, 36);
            this.kHOAComboBox.Name = "kHOAComboBox";
            this.kHOAComboBox.Size = new System.Drawing.Size(300, 21);
            this.kHOAComboBox.TabIndex = 7;
            this.kHOAComboBox.ValueMember = "MAKH";
            // 
            // lOPBindingSource1
            // 
            this.lOPBindingSource1.DataMember = "FK_LOP_KHOA1";
            this.lOPBindingSource1.DataSource = this.kHOABindingSource;
            // 
            // lOPComboBox
            // 
            this.lOPComboBox.DataSource = this.lOPBindingSource1;
            this.lOPComboBox.DisplayMember = "TENLOP";
            this.lOPComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lOPComboBox.FormattingEnabled = true;
            this.lOPComboBox.Location = new System.Drawing.Point(102, 95);
            this.lOPComboBox.Name = "lOPComboBox";
            this.lOPComboBox.Size = new System.Drawing.Size(300, 21);
            this.lOPComboBox.TabIndex = 8;
            this.lOPComboBox.ValueMember = "MALOP";
            // 
            // hOCPHIBindingSource
            // 
            this.hOCPHIBindingSource.DataMember = "HOCPHI";
            this.hOCPHIBindingSource.DataSource = this.qLDSVDataSetMain;
            // 
            // hOCPHITableAdapter
            // 
            this.hOCPHITableAdapter.ClearBeforeFill = true;
            // 
            // hOCPHIComboBox
            // 
            this.hOCPHIComboBox.DataSource = this.hOCPHIBindingSource;
            this.hOCPHIComboBox.DisplayMember = "NIENKHOA";
            this.hOCPHIComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hOCPHIComboBox.FormattingEnabled = true;
            this.hOCPHIComboBox.Location = new System.Drawing.Point(102, 122);
            this.hOCPHIComboBox.Name = "hOCPHIComboBox";
            this.hOCPHIComboBox.Size = new System.Drawing.Size(121, 21);
            this.hOCPHIComboBox.TabIndex = 8;
            this.hOCPHIComboBox.ValueMember = "NIENKHOA";
            // 
            // rpFormDSDongHP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 275);
            this.Controls.Add(this.hOCPHIComboBox);
            this.Controls.Add(this.lOPComboBox);
            this.Controls.Add(this.kHOAComboBox);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.hOCKYComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "rpFormDSDongHP";
            this.Text = "rpFormDSDongHP";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.rpFormDSDongHP_FormClosing);
            this.Load += new System.EventHandler(this.rpFormDSDongHP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSetMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lOPBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kHOABindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lOPBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hOCPHIBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private QLDSVDataSet qLDSVDataSet;
        private QLDSVDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private QLDSVDataSetMain qLDSVDataSetMain;
        private System.Windows.Forms.BindingSource lOPBindingSource;
        private QLDSVDataSetMainTableAdapters.LOPTableAdapter lOPTableAdapter;
        private QLDSVDataSetMainTableAdapters.TableAdapterManager tableAdapterManager1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox hOCKYComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.BindingSource kHOABindingSource;
        private QLDSVDataSetMainTableAdapters.KHOATableAdapter kHOATableAdapter;
        private System.Windows.Forms.ComboBox kHOAComboBox;
        private System.Windows.Forms.BindingSource lOPBindingSource1;
        private System.Windows.Forms.ComboBox lOPComboBox;
        private System.Windows.Forms.BindingSource hOCPHIBindingSource;
        private QLDSVDataSetMainTableAdapters.HOCPHITableAdapter hOCPHITableAdapter;
        private System.Windows.Forms.ComboBox hOCPHIComboBox;
    }
}