﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLDSV_CSDLPT.BaoCao
{
    public partial class rpFormPhieuDiemSV : Form
    {
        public rpFormPhieuDiemSV()
        {
            InitializeComponent();
        }

        private void rpFormPhieuDiemSV_Load(object sender, EventArgs e)
        {
            this.lOPTableAdapter.Connection.ConnectionString = Program.connectionString;
            this.sINHVIENTableAdapter.Connection.ConnectionString = Program.connectionString;
            // TODO: This line of code loads data into the 'qLDSVDataSetMain.SINHVIEN' table. You can move, or remove it, as needed.
            this.sINHVIENTableAdapter.Fill(this.qLDSVDataSetMain.SINHVIEN);
            // TODO: This line of code loads data into the 'qLDSVDataSetMain.LOP' table. You can move, or remove it, as needed.
            this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
            // TODO: This line of code loads data into the 'qLDSVDataSet.V_DSPM' table. You can move, or remove it, as needed.
            this.v_DSPMTableAdapter.Fill(this.qLDSVDataSet.V_DSPM);
            this.v_DSPMComboBox.DataSource = Program.bindingSourceKhoa;
            v_DSPMComboBox.DisplayMember = "description";
            v_DSPMComboBox.ValueMember = "subscriber_server";

            sINHVIENBindingSource.Filter = "NGHIHOC <> 'true'";

            if (Program.groupHT == "USER")
            {
                v_DSPMComboBox.Enabled = false;
            }
            else
            {
                Program.bindingSourceKhoa.Filter = "description <> 'Quản lý học phí'";
            }

        }

        private void v_DSPMComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.v_DSPMComboBox.ValueMember != "" && Program.formLogin.Visible == false)
            {
                if (Program.server != this.v_DSPMComboBox.SelectedValue.ToString())
                {
                    Program.server = this.v_DSPMComboBox.SelectedValue.ToString();
                    if (Program.userNameHTai != Program.remoteLogin)
                    {
                        Program.userNameHTai = Program.remoteLogin;
                        Program.passwordHTai = Program.remotePassword;
                    }
                    else
                    {
                        Program.userNameHTai = Program.userName;
                        Program.passwordHTai = Program.password;
                    }
                    Program.connectionString = "Server = " + Program.server + "; Database = " + Program.database + "; User Id = "
                        + Program.userNameHTai + "; Password = " + Program.passwordHTai;
                    Program.sqlConnection = new SqlConnection(Program.connectionString);
                    Program.sqlConnection.Open();
                    this.lOPTableAdapter.Connection.ConnectionString = Program.connectionString;
                    this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
                    this.sINHVIENTableAdapter.Connection.ConnectionString = Program.connectionString;
                    this.sINHVIENTableAdapter.Fill(this.qLDSVDataSetMain.SINHVIEN);
                    sINHVIENBindingSource.Filter = "NGHIHOC <> 'true'";
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConnection = new SqlConnection(Program.connectionString);
            sqlConnection.Open();
            string sql = "EXEC SP_TIMSV '" + sINHVIENComboBox.Text + "'";

            try
            {
                SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection);
                SqlDataReader dataReader = sqlCommand.ExecuteReader();
                dataReader.Read();

                RpPhieuDiemSV rpt = new RpPhieuDiemSV(sINHVIENComboBox.Text);
                rpt.xrMASV.Text += sINHVIENComboBox.Text;
                hOTextEdit.Visible = tENTextEdit.Visible = true;
                rpt.xrTEN.Text += dataReader.GetString(0) + " " + dataReader.GetString(1);
                hOTextEdit.Visible = tENTextEdit.Visible = false;
                rpt.xrLop.Text += dataReader.GetString(2);
                ReportPrintTool print = new ReportPrintTool(rpt);
                print.ShowPreviewDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Không có sinh viên này!");
                return;
            }
            //this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
            //this.sINHVIENTableAdapter.Fill(this.qLDSVDataSetMain.SINHVIEN);
            //sINHVIENBindingSource.Filter = "NGHIHOC <> 'true'";
        }

        private void rpFormPhieuDiemSV_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.rpFormPDSV = null;
        }
    }
}
