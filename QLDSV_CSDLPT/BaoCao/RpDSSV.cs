﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QLDSV_CSDLPT.BaoCao
{
    public partial class RpDSSV : DevExpress.XtraReports.UI.XtraReport
    {
        public RpDSSV(string maLop)
        {
            InitializeComponent();
            //this.DataSource = this.qldsvDataSetMain1.SP_INDANHSACHSINHVIEN;
            this.sP_INDANHSACHSINHVIENTableAdapter1.Connection.ConnectionString = Program.connectionString;
            this.sP_INDANHSACHSINHVIENTableAdapter1.Fill(qldsvDataSetMain1.SP_INDANHSACHSINHVIEN, maLop);
            if (this.qldsvDataSetMain1.SP_INDANHSACHSINHVIEN.Count == 0) xrLop.Text = "false";
            //this.sP_DSHOCPHICUALOPTableAdapter1.Fill(qldsvDataSetMain1.SP_DSHOCPHICUALOP, "d15cqcn1", "2015-2016", 1);
        }

    }
}
