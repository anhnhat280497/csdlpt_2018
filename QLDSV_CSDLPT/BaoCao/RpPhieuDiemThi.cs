﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QLDSV_CSDLPT.BaoCao
{
    public partial class RpPhieuDiemThi : DevExpress.XtraReports.UI.XtraReport
    {
        public RpPhieuDiemThi(string lop, string mon, int hocky, DateTime ngaythi, int lanthi)
        {
            InitializeComponent();
            //this.DataSource = this.qldsvDataSetMain1.SP_PHIEUDIEMTHI;
            this.sP_PHIEUDIEMTHITableAdapter1.Connection.ConnectionString = Program.connectionString;
            this.sP_PHIEUDIEMTHITableAdapter1.Fill(qldsvDataSetMain1.SP_PHIEUDIEMTHI, lop, mon, hocky, ngaythi, lanthi);
            if (this.qldsvDataSetMain1.SP_PHIEUDIEMTHI.Count == 0) xrLop.Text = "false";
            //this.sP_PHIEUDIEMTHITableAdapter1.Fill(qldsvDataSetMain1.SP_PHIEUDIEMTHI, "Cao đẳng", "Cau truc du lieu", 1, null, 1);
        }

    }
}
