﻿namespace QLDSV_CSDLPT.BaoCao
{
    partial class rpFormBangDiemMH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.qLDSVDataSet = new QLDSV_CSDLPT.QLDSVDataSet();
            this.v_DSPMBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.v_DSPMTableAdapter = new QLDSV_CSDLPT.QLDSVDataSetTableAdapters.V_DSPMTableAdapter();
            this.tableAdapterManager = new QLDSV_CSDLPT.QLDSVDataSetTableAdapters.TableAdapterManager();
            this.label1 = new System.Windows.Forms.Label();
            this.qLDSVDataSetMain = new QLDSV_CSDLPT.QLDSVDataSetMain();
            this.lOPBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lOPTableAdapter = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.LOPTableAdapter();
            this.tableAdapterManager1 = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.TableAdapterManager();
            this.mONHOCTableAdapter = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.MONHOCTableAdapter();
            this.label2 = new System.Windows.Forms.Label();
            this.mONHOCBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mONHOCComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lANTHIComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.v_DSPMBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.v_DSPMComboBox = new System.Windows.Forms.ComboBox();
            this.lOPBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.lOPComboBox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_DSPMBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSetMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lOPBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mONHOCBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_DSPMBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lOPBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // qLDSVDataSet
            // 
            this.qLDSVDataSet.DataSetName = "QLDSVDataSet";
            this.qLDSVDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // v_DSPMBindingSource
            // 
            this.v_DSPMBindingSource.DataMember = "V_DSPM";
            this.v_DSPMBindingSource.DataSource = this.qLDSVDataSet;
            // 
            // v_DSPMTableAdapter
            // 
            this.v_DSPMTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = QLDSV_CSDLPT.QLDSVDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(66, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "KHOA:";
            // 
            // qLDSVDataSetMain
            // 
            this.qLDSVDataSetMain.DataSetName = "QLDSVDataSetMain";
            this.qLDSVDataSetMain.EnforceConstraints = false;
            this.qLDSVDataSetMain.Locale = new System.Globalization.CultureInfo("en-US");
            this.qLDSVDataSetMain.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lOPBindingSource
            // 
            this.lOPBindingSource.DataMember = "LOP";
            this.lOPBindingSource.DataSource = this.qLDSVDataSetMain;
            // 
            // lOPTableAdapter
            // 
            this.lOPTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.DIEMTableAdapter = null;
            this.tableAdapterManager1.GIANGVIENTableAdapter = null;
            this.tableAdapterManager1.HOCPHITableAdapter = null;
            this.tableAdapterManager1.KHOATableAdapter = null;
            this.tableAdapterManager1.LOPTableAdapter = this.lOPTableAdapter;
            this.tableAdapterManager1.MONHOCTableAdapter = this.mONHOCTableAdapter;
            this.tableAdapterManager1.SINHVIENTableAdapter = null;
            this.tableAdapterManager1.UpdateOrder = QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // mONHOCTableAdapter
            // 
            this.mONHOCTableAdapter.ClearBeforeFill = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "LỚP:";
            // 
            // mONHOCBindingSource
            // 
            this.mONHOCBindingSource.DataMember = "MONHOC";
            this.mONHOCBindingSource.DataSource = this.qLDSVDataSetMain;
            // 
            // mONHOCComboBox
            // 
            this.mONHOCComboBox.DataSource = this.mONHOCBindingSource;
            this.mONHOCComboBox.DisplayMember = "TENMH";
            this.mONHOCComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mONHOCComboBox.FormattingEnabled = true;
            this.mONHOCComboBox.Location = new System.Drawing.Point(115, 112);
            this.mONHOCComboBox.Name = "mONHOCComboBox";
            this.mONHOCComboBox.Size = new System.Drawing.Size(328, 21);
            this.mONHOCComboBox.TabIndex = 4;
            this.mONHOCComboBox.ValueMember = "TENMH";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "MÔN:";
            // 
            // lANTHIComboBox
            // 
            this.lANTHIComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lANTHIComboBox.FormattingEnabled = true;
            this.lANTHIComboBox.Location = new System.Drawing.Point(115, 149);
            this.lANTHIComboBox.Name = "lANTHIComboBox";
            this.lANTHIComboBox.Size = new System.Drawing.Size(121, 21);
            this.lANTHIComboBox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(66, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "LẦN THI:";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(247, 196);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // v_DSPMBindingSource1
            // 
            this.v_DSPMBindingSource1.DataMember = "V_DSPM";
            this.v_DSPMBindingSource1.DataSource = this.qLDSVDataSet;
            // 
            // v_DSPMComboBox
            // 
            this.v_DSPMComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.v_DSPMComboBox.FormattingEnabled = true;
            this.v_DSPMComboBox.Location = new System.Drawing.Point(115, 39);
            this.v_DSPMComboBox.Name = "v_DSPMComboBox";
            this.v_DSPMComboBox.Size = new System.Drawing.Size(328, 21);
            this.v_DSPMComboBox.TabIndex = 7;
            this.v_DSPMComboBox.SelectedIndexChanged += new System.EventHandler(this.v_DSPMComboBox_SelectedIndexChanged);
            // 
            // lOPBindingSource1
            // 
            this.lOPBindingSource1.DataMember = "LOP";
            this.lOPBindingSource1.DataSource = this.qLDSVDataSetMain;
            // 
            // lOPComboBox
            // 
            this.lOPComboBox.DataSource = this.lOPBindingSource1;
            this.lOPComboBox.DisplayMember = "TENLOP";
            this.lOPComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lOPComboBox.FormattingEnabled = true;
            this.lOPComboBox.Location = new System.Drawing.Point(115, 75);
            this.lOPComboBox.Name = "lOPComboBox";
            this.lOPComboBox.Size = new System.Drawing.Size(328, 21);
            this.lOPComboBox.TabIndex = 8;
            this.lOPComboBox.ValueMember = "MALOP";
            // 
            // rpFormBangDiemMH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 247);
            this.Controls.Add(this.lOPComboBox);
            this.Controls.Add(this.v_DSPMComboBox);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lANTHIComboBox);
            this.Controls.Add(this.mONHOCComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "rpFormBangDiemMH";
            this.Text = "rpFormBangDiemMH";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.rpFormBangDiemMH_FormClosing);
            this.Load += new System.EventHandler(this.rpFormBangDiemMH_Load);
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_DSPMBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSetMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lOPBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mONHOCBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_DSPMBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lOPBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private QLDSVDataSet qLDSVDataSet;
        private System.Windows.Forms.BindingSource v_DSPMBindingSource;
        private QLDSVDataSetTableAdapters.V_DSPMTableAdapter v_DSPMTableAdapter;
        private QLDSVDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Label label1;
        private QLDSVDataSetMain qLDSVDataSetMain;
        private System.Windows.Forms.BindingSource lOPBindingSource;
        private QLDSVDataSetMainTableAdapters.LOPTableAdapter lOPTableAdapter;
        private QLDSVDataSetMainTableAdapters.TableAdapterManager tableAdapterManager1;
        private QLDSVDataSetMainTableAdapters.MONHOCTableAdapter mONHOCTableAdapter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource mONHOCBindingSource;
        private System.Windows.Forms.ComboBox mONHOCComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox lANTHIComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.BindingSource v_DSPMBindingSource1;
        private System.Windows.Forms.ComboBox v_DSPMComboBox;
        private System.Windows.Forms.BindingSource lOPBindingSource1;
        private System.Windows.Forms.ComboBox lOPComboBox;
    }
}