﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QLDSV_CSDLPT.BaoCao
{
    public partial class RpPhieuDiemSV : DevExpress.XtraReports.UI.XtraReport
    {
        public RpPhieuDiemSV(string masv)
        {
            InitializeComponent();
            //this.DataSource = this.qldsvDataSetMain1.SP_PHIEUDIEMSV;
            //this.sP_PHIEUDIEMSVTableAdapter1.Fill(qldsvDataSetMain1.SP_PHIEUDIEMSV, "n15dccn001");
            this.sP_PHIEUDIEMSVTableAdapter1.Connection.ConnectionString = Program.connectionString;
            this.sP_PHIEUDIEMSVTableAdapter1.Fill(qldsvDataSetMain1.SP_PHIEUDIEMSV,masv);
            if (qldsvDataSetMain1.SP_PHIEUDIEMSV.Count == 0) xrLop.Text = "false";
        }

    }
}
