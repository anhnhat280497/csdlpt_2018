﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using System.Data.SqlClient;

namespace QLDSV_CSDLPT.BaoCao
{
    public partial class XtraForm1 : DevExpress.XtraEditors.XtraForm
    {
        public XtraForm1()
        {
            InitializeComponent();
        }

        private void XtraForm1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qLDSVDataSet.V_DSPM' table. You can move, or remove it, as needed.
            this.v_DSPMTableAdapter.Fill(this.qLDSVDataSet.V_DSPM);
            // TODO: This line of code loads data into the 'qLDSVDataSetMain.LOP' table. You can move, or remove it, as needed.
            this.lOPTableAdapter.Connection.ConnectionString = Program.connectionString;
            this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);

            this.DSPMComboBox.DataSource = Program.bindingSourceKhoa;
            DSPMComboBox.DisplayMember = "description";
            DSPMComboBox.ValueMember = "subscriber_server";

            if (Program.groupHT == "USER")
            {
                DSPMComboBox.Enabled = false;
            }
            else
            {
                Program.bindingSourceKhoa.Filter = "description <> 'Quản lý học phí'";
            }

        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            string malop = mALOPLabel.Text;
            try
            {
                RpDSSV rpt = new RpDSSV(malop);
                if (rpt.xrLop.Text == "false")
                {
                    MessageBox.Show("Lớp không có sinh viên!");
                    return;
                }
                rpt.xrLop.Text = "LỚP: " + this.tENLOPComboBox.Text;
                ReportPrintTool print = new ReportPrintTool(rpt);
                print.ShowPreviewDialog();
                this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            
        }

        private void XtraForm1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.inDanhSachSinhVien = null;
        }

        private void DSPMComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.DSPMComboBox.ValueMember != "" && Program.formLogin.Visible == false)
            {
                if (Program.server != this.DSPMComboBox.SelectedValue.ToString())
                {
                    Program.server = this.DSPMComboBox.SelectedValue.ToString();
                    if (Program.userNameHTai != Program.remoteLogin)
                    {
                        Program.userNameHTai = Program.remoteLogin;
                        Program.passwordHTai = Program.remotePassword;
                    }
                    else
                    {
                        Program.userNameHTai = Program.userName;
                        Program.passwordHTai = Program.password;
                    }
                    Program.connectionString = "Server = " + Program.server + "; Database = " + Program.database + "; User Id = "
                        + Program.userNameHTai + "; Password = " + Program.passwordHTai;
                    Program.sqlConnection = new SqlConnection(Program.connectionString);
                    Program.sqlConnection.Open();
                    this.lOPTableAdapter.Connection.ConnectionString = Program.connectionString;
                    this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
                }
            }
        }
    }
}