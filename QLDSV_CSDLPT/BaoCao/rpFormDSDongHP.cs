﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLDSV_CSDLPT.BaoCao
{
    public partial class rpFormDSDongHP : Form
    {
        public rpFormDSDongHP()
        {
            InitializeComponent();
        }

        private void rpFormDSDongHP_Load(object sender, EventArgs e)
        {
            this.hOCPHITableAdapter.Connection.ConnectionString = Program.connectionString;
            // TODO: This line of code loads data into the 'qLDSVDataSetMain.HOCPHI' table. You can move, or remove it, as needed.
            this.hOCPHITableAdapter.Fill(this.qLDSVDataSetMain.HOCPHI);
            // TODO: This line of code loads data into the 'qLDSVDataSetMain.KHOA' table. You can move, or remove it, as needed.
            this.kHOATableAdapter.Fill(this.qLDSVDataSetMain.KHOA);
            this.lOPTableAdapter.Connection.ConnectionString = Program.connectionString;
            // TODO: This line of code loads data into the 'qLDSVDataSetMain.LOP' table. You can move, or remove it, as needed.
            this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
            // TODO: This line of code loads data into the 'qLDSVDataSet.V_DSPM' table. You can move, or remove it, as needed.
            
            if (Program.groupHT == "USER")
            {
                kHOAComboBox.Enabled = false;
            }
            else
            {
                Program.bindingSourceKhoa.Filter = "description <> 'Quản lý học phí'";
            }

            IDictionary<int, string> dict = new Dictionary<int, string>();
            dict.Add(1, "1");
            dict.Add(2, "2");
            hOCKYComboBox.DataSource = new BindingSource(dict, null);
            hOCKYComboBox.DisplayMember = "Value";
            hOCKYComboBox.ValueMember = "Key";
        }
        

        private void btnOK_Click(object sender, EventArgs e)
        {
            string lop = lOPComboBox.SelectedValue.ToString(), nienkhoa = hOCPHIComboBox.Text;
            int hocky = Int16.Parse(hOCKYComboBox.Text);
            try
            {
                RpDSDongHP rpt = new RpDSDongHP(lop, nienkhoa, hocky);
                rpt.xrLOP.Text += lop;
                rpt.xrNIENKHOA.Text += nienkhoa;
                rpt.xrHOCKY.Text += hocky;
                ReportPrintTool print = new ReportPrintTool(rpt);
                print.ShowPreviewDialog();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                MessageBox.Show("Lớp không có sinh viên!" + ex.Message);
            }
            //this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
        }

        private void rpFormDSDongHP_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.rpFormDSHP = null;
        }
    }
}
