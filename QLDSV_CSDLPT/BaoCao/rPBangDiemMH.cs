﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QLDSV_CSDLPT.BaoCao
{
    public partial class rPBangDiemMH : DevExpress.XtraReports.UI.XtraReport
    {
        public rPBangDiemMH(string lop, string mon, int lan)
        {
            InitializeComponent();
            //this.DataSource = this.qldsvDataSetMain1.SP_BANGDIEMMONHOC;
            this.sP_BANGDIEMMONHOCTableAdapter1.Connection.ConnectionString = Program.connectionString;
            this.sP_BANGDIEMMONHOCTableAdapter1.Fill(qldsvDataSetMain1.SP_BANGDIEMMONHOC, lop, mon, lan);
            if (qldsvDataSetMain1.SP_BANGDIEMMONHOC.Count == 0) xrLop.Text = "false";
            //this.sP_BANGDIEMMONHOCTableAdapter1.Fill(qldsvDataSetMain1.SP_BANGDIEMMONHOC, "Cao đẳng", "Cau truc du lieu", 1);
        }

    }
}
