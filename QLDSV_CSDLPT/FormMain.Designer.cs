﻿namespace QLDSV_CSDLPT
{
    partial class formMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formMain));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barCheckItem1 = new DevExpress.XtraBars.BarCheckItem();
            this.btnBarMH = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.btnBarSV = new DevExpress.XtraBars.BarButtonItem();
            this.btnBarLop = new DevExpress.XtraBars.BarButtonItem();
            this.btnBarDiem = new DevExpress.XtraBars.BarButtonItem();
            this.btnBarReportDSSV = new DevExpress.XtraBars.BarButtonItem();
            this.btnBarHP = new DevExpress.XtraBars.BarButtonItem();
            this.barDSSVThiHetMon = new DevExpress.XtraBars.BarButtonItem();
            this.barBangDiemMH = new DevExpress.XtraBars.BarButtonItem();
            this.barPhieuDiemSV = new DevExpress.XtraBars.BarButtonItem();
            this.barDongHPCuaLop = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageCategory1 = new DevExpress.XtraBars.Ribbon.RibbonPageCategory();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.svPage = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.gvPage = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.diemPage = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageReport = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.statusGV = new System.Windows.Forms.StatusStrip();
            this.labelMaGV = new System.Windows.Forms.ToolStripStatusLabel();
            this.labelNameGV = new System.Windows.Forms.ToolStripStatusLabel();
            this.labelGroupGV = new System.Windows.Forms.ToolStripStatusLabel();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.ribbonPage4 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPage5 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            this.statusGV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.AutoHideEmptyItems = true;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItem1,
            this.barCheckItem1,
            this.btnBarMH,
            this.barButtonItem3,
            this.btnBarSV,
            this.btnBarLop,
            this.btnBarDiem,
            this.btnBarReportDSSV,
            this.btnBarHP,
            this.barDSSVThiHetMon,
            this.barBangDiemMH,
            this.barPhieuDiemSV,
            this.barDongHPCuaLop});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 14;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.PageCategories.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageCategory[] {
            this.ribbonPageCategory1});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbonControl1.Size = new System.Drawing.Size(758, 143);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barCheckItem1
            // 
            this.barCheckItem1.Caption = "barCheckItem1";
            this.barCheckItem1.Id = 2;
            this.barCheckItem1.Name = "barCheckItem1";
            // 
            // btnBarMH
            // 
            this.btnBarMH.Caption = "MÔN HỌC";
            this.btnBarMH.Id = 3;
            this.btnBarMH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBarMH.ImageOptions.Image")));
            this.btnBarMH.Name = "btnBarMH";
            this.btnBarMH.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnBarMH_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "barButtonItem3";
            this.barButtonItem3.Id = 4;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // btnBarSV
            // 
            this.btnBarSV.Caption = "SINH VIÊN";
            this.btnBarSV.Id = 5;
            this.btnBarSV.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBarSV.ImageOptions.Image")));
            this.btnBarSV.Name = "btnBarSV";
            this.btnBarSV.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnBarSV_ItemClick);
            // 
            // btnBarLop
            // 
            this.btnBarLop.Caption = "LỚP";
            this.btnBarLop.Id = 6;
            this.btnBarLop.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBarLop.ImageOptions.Image")));
            this.btnBarLop.Name = "btnBarLop";
            this.btnBarLop.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnBarLop_ItemClick);
            // 
            // btnBarDiem
            // 
            this.btnBarDiem.Caption = "ĐIỂM";
            this.btnBarDiem.Id = 7;
            this.btnBarDiem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBarDiem.ImageOptions.Image")));
            this.btnBarDiem.Name = "btnBarDiem";
            this.btnBarDiem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnBarDiem_ItemClick);
            // 
            // btnBarReportDSSV
            // 
            this.btnBarReportDSSV.Caption = "DSSV Theo Lớp";
            this.btnBarReportDSSV.Id = 8;
            this.btnBarReportDSSV.Name = "btnBarReportDSSV";
            this.btnBarReportDSSV.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnBarReportDSSV_ItemClick);
            // 
            // btnBarHP
            // 
            this.btnBarHP.Caption = "HỌC PHÍ";
            this.btnBarHP.Id = 9;
            this.btnBarHP.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBarHP.ImageOptions.Image")));
            this.btnBarHP.Name = "btnBarHP";
            this.btnBarHP.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnBarHP_ItemClick);
            // 
            // barDSSVThiHetMon
            // 
            this.barDSSVThiHetMon.Caption = "DSSV Thi Hết Môn";
            this.barDSSVThiHetMon.Id = 10;
            this.barDSSVThiHetMon.Name = "barDSSVThiHetMon";
            this.barDSSVThiHetMon.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barBangDiemMH
            // 
            this.barBangDiemMH.Caption = "Bảng Điểm Môn Học";
            this.barBangDiemMH.Id = 11;
            this.barBangDiemMH.Name = "barBangDiemMH";
            this.barBangDiemMH.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // barPhieuDiemSV
            // 
            this.barPhieuDiemSV.Caption = "Phiếu Điểm Sinh Viên";
            this.barPhieuDiemSV.Id = 12;
            this.barPhieuDiemSV.Name = "barPhieuDiemSV";
            this.barPhieuDiemSV.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem5_ItemClick);
            // 
            // barDongHPCuaLop
            // 
            this.barDongHPCuaLop.Caption = "DS Đóng Học Phí Của Lớp";
            this.barDongHPCuaLop.Id = 13;
            this.barDongHPCuaLop.Name = "barDongHPCuaLop";
            this.barDongHPCuaLop.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // ribbonPageCategory1
            // 
            this.ribbonPageCategory1.Name = "ribbonPageCategory1";
            this.ribbonPageCategory1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage3,
            this.ribbonPageReport});
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.svPage,
            this.gvPage,
            this.diemPage,
            this.ribbonPageGroup3});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "Danh Mục";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnBarLop);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            // 
            // svPage
            // 
            this.svPage.ItemLinks.Add(this.btnBarSV);
            this.svPage.Name = "svPage";
            // 
            // gvPage
            // 
            this.gvPage.ItemLinks.Add(this.btnBarMH);
            this.gvPage.Name = "gvPage";
            // 
            // diemPage
            // 
            this.diemPage.ItemLinks.Add(this.btnBarDiem);
            this.diemPage.Name = "diemPage";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btnBarHP);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            // 
            // ribbonPageReport
            // 
            this.ribbonPageReport.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2,
            this.ribbonPageGroup4,
            this.ribbonPageGroup5,
            this.ribbonPageGroup6,
            this.ribbonPageGroup7});
            this.ribbonPageReport.Name = "ribbonPageReport";
            this.ribbonPageReport.Text = "Báo cáo";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnBarReportDSSV);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.barDSSVThiHetMon);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.barBangDiemMH);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.barPhieuDiemSV);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.barDongHPCuaLop);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            // 
            // statusGV
            // 
            this.statusGV.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.labelMaGV,
            this.labelNameGV,
            this.labelGroupGV});
            this.statusGV.Location = new System.Drawing.Point(0, 338);
            this.statusGV.Name = "statusGV";
            this.statusGV.Size = new System.Drawing.Size(758, 22);
            this.statusGV.TabIndex = 1;
            this.statusGV.Text = "statusStrip1";
            // 
            // labelMaGV
            // 
            this.labelMaGV.Name = "labelMaGV";
            this.labelMaGV.Size = new System.Drawing.Size(50, 17);
            this.labelMaGV.Text = "MÃ GV: ";
            // 
            // labelNameGV
            // 
            this.labelNameGV.Name = "labelNameGV";
            this.labelNameGV.Size = new System.Drawing.Size(35, 17);
            this.labelNameGV.Text = "TÊN: ";
            // 
            // labelGroupGV
            // 
            this.labelGroupGV.Name = "labelGroupGV";
            this.labelGroupGV.Size = new System.Drawing.Size(51, 17);
            this.labelGroupGV.Text = "NHÓM: ";
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // ribbonPage4
            // 
            this.ribbonPage4.Name = "ribbonPage4";
            this.ribbonPage4.Text = "ribbonPage4";
            // 
            // ribbonPage5
            // 
            this.ribbonPage5.Name = "ribbonPage5";
            this.ribbonPage5.Text = "ribbonPage5";
            // 
            // formMain
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 360);
            this.Controls.Add(this.statusGV);
            this.Controls.Add(this.ribbonControl1);
            this.IsMdiContainer = true;
            this.Name = "formMain";
            this.Ribbon = this.ribbonControl1;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ĐIỂM SINH VIÊN";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.statusGV.ResumeLayout(false);
            this.statusGV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private System.Windows.Forms.StatusStrip statusGV;
        private System.Windows.Forms.ToolStripStatusLabel labelMaGV;
        private System.Windows.Forms.ToolStripStatusLabel labelNameGV;
        private System.Windows.Forms.ToolStripStatusLabel labelGroupGV;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage4;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarCheckItem barCheckItem1;
        private DevExpress.XtraBars.BarButtonItem btnBarMH;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.Ribbon.RibbonPageCategory ribbonPageCategory1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup gvPage;
        private DevExpress.XtraBars.BarButtonItem btnBarSV;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup svPage;
        private DevExpress.XtraBars.BarButtonItem btnBarLop;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem btnBarDiem;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup diemPage;
        private DevExpress.XtraBars.BarButtonItem btnBarReportDSSV;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageReport;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem btnBarHP;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem barDSSVThiHetMon;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.BarButtonItem barBangDiemMH;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.BarButtonItem barPhieuDiemSV;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.BarButtonItem barDongHPCuaLop;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
    }
}

