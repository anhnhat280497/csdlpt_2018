﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLDSV_CSDLPT
{
    public partial class formMH : Form
    {
        Boolean edit = false, okInsert = false;
        public formMH()
        {
            InitializeComponent();
        }

        private void FormMH_Load(object sender, EventArgs e)
        {
            this.dIEMTableAdapter.Connection.ConnectionString 
                = "Data Source=DESKTOP-4E8JQT4;Initial Catalog=QLDSV;Persist Security Info=True;User ID=sa;Password=sa;";
            //Program.connectionString;
            // TODO: This line of code loads data into the 'qLDSVDataSetMain.DIEM' table. You can move, or remove it, as needed.
            this.dIEMTableAdapter.Fill(this.qLDSVDataSetMain.DIEM);
            // TODO: This line of code loads data into the 'qLDSVDataSetMain.GIANGVIEN' table. You can move, or remove it, as needed.
            this.mONHOCTableAdapter.Connection.ConnectionString = Program.connectionString;
            this.mONHOCTableAdapter.Fill(this.qLDSVDataSetMain.MONHOC);
            if (mONHOCBindingSource.Count == 0) btnDelete.Enabled = false;
            if (Program.groupHT == "USER")
            {
                btnDelete.Enabled = btnEdit.Enabled = btnInSert.Enabled = false;
            }
        }

        private void btnInSert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.mONHOCBindingSource.AddNew();
            this.mONHOCGridControl.Enabled = btnDelete.Enabled = btnEdit.Enabled =
                btnInSert.Enabled = btnRefresh.Enabled = btnSave.Enabled = false;
            btnUndo.Enabled = panelText.Enabled = btnHoantat.Enabled = true;
        }

        private void btnSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                this.mONHOCTableAdapter.Update(this.qLDSVDataSetMain.MONHOC);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi lưu môn.\n" + ex.Message, "", MessageBoxButtons.OK);
                return;
            }
            this.mONHOCGridControl.Enabled = btnDelete.Enabled = btnEdit.Enabled =
                btnInSert.Enabled = btnRefresh.Enabled = true;
            btnSave.Enabled = btnUndo.Enabled = panelText.Enabled = edit = false;
        }

        private void btnRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                this.mONHOCTableAdapter.Fill(this.qLDSVDataSetMain.MONHOC);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi Refresh :" + ex.Message, "", MessageBoxButtons.OK);
                return;
            }
        }

        private void btnDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            String maMon = "";
            if (dIEMBindingSource.Count > 0)
            {
                MessageBox.Show("Không thể xóa môn này vì đã nhập điểm cho sinh viên ", "",
                       MessageBoxButtons.OK);
                return;
            }
            if (MessageBox.Show("Bạn có thật sự muốn xóa môn này ?? ", "Xác nhận",
                       MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    maMon = ((DataRowView)mONHOCBindingSource[mONHOCBindingSource.Position])["MAMH"].ToString(); // giữ lại để khi xóa bij lỗi thì ta sẽ quay về lại
                    mONHOCBindingSource.RemoveCurrent();
                    this.mONHOCTableAdapter.Connection.ConnectionString = Program.connectionString;
                    this.mONHOCTableAdapter.Update(this.qLDSVDataSetMain.MONHOC);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Lỗi xóa môn. Bạn hãy xóa lại\n" + ex.Message, "",
                        MessageBoxButtons.OK);
                    this.mONHOCTableAdapter.Fill(this.qLDSVDataSetMain.MONHOC);
                    mONHOCBindingSource.Position = mONHOCBindingSource.Find("MAMH", maMon);
                    return;
                }
            }

        }

        private void btnEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btnDelete.Enabled = btnEdit.Enabled = btnInSert.Enabled = btnRefresh.Enabled =
                mONHOCGridControl.Enabled = btnSave.Enabled = btnHoantat.Enabled = false;
            panelText.Enabled = btnUndo.Enabled = edit = true;
        }

        private void btnUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.mONHOCBindingSource.CancelEdit();
            this.mONHOCTableAdapter.Fill(qLDSVDataSetMain.MONHOC);
            btnRefresh.Enabled = btnEdit.Enabled = btnInSert.Enabled = btnDelete.Enabled =
                this.mONHOCGridControl.Enabled = true;
            panelText.Enabled = btnSave.Enabled = btnUndo.Enabled = false;
            if (okInsert)
            {
                this.mONHOCGridControl.Enabled = btnRefresh.Enabled =
                    btnEdit.Enabled = btnInSert.Enabled = btnDelete.Enabled = false;
                panelText.Enabled = true;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string maMH = mAMHTextEdit.Text;
            if (maMH == "")
            {
                MessageBox.Show("Mã môn không được trống!", "", MessageBoxButtons.OK);
                mAMHTextEdit.Focus();
                return;
            }
            if (tENMHTextEdit.Text == "")
            {
                MessageBox.Show("Tên môn không được trống!", "", MessageBoxButtons.OK);
                tENMHTextEdit.Focus();
                return;
            }
            int demtrung = 0;
            for (int i = 0; i < mONHOCBindingSource.Count; i++)
            {
                if (maMH == ((DataRowView)mONHOCBindingSource[i])["MAMH"].ToString().Trim())
                {
                    demtrung++;
                }
            }
            if (demtrung == 2)
            {
                MessageBox.Show("Mã môn bị trùng!");
                mAMHTextEdit.Focus();
                return;
            }
            this.mONHOCBindingSource.EndEdit();
            this.mONHOCBindingSource.ResetCurrentItem();
            btnUndo.Enabled = false;
            if(!edit)
            {
                this.mONHOCBindingSource.AddNew();
                okInsert = true;
            }
            else
            {
                btnSave.Enabled = true;
                panelText.Enabled = false;
            }
        }

        private void btnHoantat_Click(object sender, EventArgs e)
        {
            this.mONHOCBindingSource.RemoveAt(mONHOCBindingSource.Count - 1);
            btnSave.Enabled = true;
            panelText.Enabled = btnUndo.Enabled = false;
        }

        private void formMH_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.formMH = null;
        }

        private void btnExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }
    }
}
