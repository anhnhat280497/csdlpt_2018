﻿namespace QLDSV_CSDLPT
{
    partial class FormDIEM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDIEM));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar15 = new DevExpress.XtraBars.Bar();
            this.btnExit = new DevExpress.XtraBars.BarButtonItem();
            this.bar16 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.qLDSVDataSetMain = new QLDSV_CSDLPT.QLDSVDataSetMain();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.bar17 = new DevExpress.XtraBars.Bar();
            this.qLDSVDataSet = new QLDSV_CSDLPT.QLDSVDataSet();
            this.v_DSPMTableAdapter = new QLDSV_CSDLPT.QLDSVDataSetTableAdapters.V_DSPMTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.colTEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.bar5 = new DevExpress.XtraBars.Bar();
            this.bar6 = new DevExpress.XtraBars.Bar();
            this.bar7 = new DevExpress.XtraBars.Bar();
            this.bar8 = new DevExpress.XtraBars.Bar();
            this.bar10 = new DevExpress.XtraBars.Bar();
            this.bar11 = new DevExpress.XtraBars.Bar();
            this.bar12 = new DevExpress.XtraBars.Bar();
            this.bar13 = new DevExpress.XtraBars.Bar();
            this.bar14 = new DevExpress.XtraBars.Bar();
            this.bar9 = new DevExpress.XtraBars.Bar();
            this.tableAdapterManager = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.TableAdapterManager();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBoxKhoa = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panlFill = new System.Windows.Forms.Panel();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnXong = new System.Windows.Forms.Button();
            this.grFill = new System.Windows.Forms.GroupBox();
            this.mONHOCComboBox = new System.Windows.Forms.ComboBox();
            this.mONHOCBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lOPComboBox = new System.Windows.Forms.ComboBox();
            this.lOPBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.comboLanThi = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnNhapDiem = new System.Windows.Forms.Button();
            this.lOPTableAdapter = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.LOPTableAdapter();
            this.mONHOCTableAdapter = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.MONHOCTableAdapter();
            this.sP_SUADIEMMONHOCBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sP_SUADIEMMONHOCTableAdapter = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.SP_SUADIEMMONHOCTableAdapter();
            this.sP_NHAPDIEMSINHVIENBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sP_NHAPDIEMSINHVIENTableAdapter = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.SP_NHAPDIEMSINHVIENTableAdapter();
            this.sP_NHAPDIEMSINHVIENGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMASV1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHOTEN1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDIEM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sP_SUADIEMMONHOCGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMASV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHOTEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDIEM = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSetMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panlFill.SuspendLayout();
            this.grFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mONHOCBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lOPBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sP_SUADIEMMONHOCBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sP_NHAPDIEMSINHVIENBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sP_NHAPDIEMSINHVIENGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sP_SUADIEMMONHOCGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar15,
            this.bar16});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.btnExit});
            this.barManager1.MainMenu = this.bar15;
            this.barManager1.MaxItemId = 7;
            this.barManager1.StatusBar = this.bar16;
            // 
            // bar15
            // 
            this.bar15.BarName = "Main menu";
            this.bar15.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar15.DockCol = 0;
            this.bar15.DockRow = 0;
            this.bar15.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar15.FloatLocation = new System.Drawing.Point(84, 90);
            this.bar15.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar15.OptionsBar.MultiLine = true;
            this.bar15.OptionsBar.UseWholeRow = true;
            this.bar15.Text = "Main menu";
            // 
            // btnExit
            // 
            this.btnExit.Caption = "Exit";
            this.btnExit.Id = 6;
            this.btnExit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.ImageOptions.Image")));
            this.btnExit.Name = "btnExit";
            this.btnExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExit_ItemClick);
            // 
            // bar16
            // 
            this.bar16.BarName = "Status bar";
            this.bar16.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar16.DockCol = 0;
            this.bar16.DockRow = 0;
            this.bar16.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar16.OptionsBar.AllowQuickCustomization = false;
            this.bar16.OptionsBar.DrawDragBorder = false;
            this.bar16.OptionsBar.UseWholeRow = true;
            this.bar16.Text = "Status bar";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(910, 40);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 434);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(910, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 40);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 394);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(910, 40);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 394);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Insert";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Delete";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.Image")));
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Edit";
            this.barButtonItem3.Id = 2;
            this.barButtonItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.Image")));
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Save";
            this.barButtonItem4.Id = 3;
            this.barButtonItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.Image")));
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Undo";
            this.barButtonItem5.Id = 4;
            this.barButtonItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.ImageOptions.Image")));
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Refresh";
            this.barButtonItem6.Id = 5;
            this.barButtonItem6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem6.ImageOptions.Image")));
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // qLDSVDataSetMain
            // 
            this.qLDSVDataSetMain.DataSetName = "QLDSVDataSetMain";
            this.qLDSVDataSetMain.EnforceConstraints = false;
            this.qLDSVDataSetMain.Locale = new System.Globalization.CultureInfo("en-US");
            this.qLDSVDataSetMain.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bar17
            // 
            this.bar17.BarName = "Main menu";
            this.bar17.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar17.DockCol = 0;
            this.bar17.DockRow = 0;
            this.bar17.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar17.FloatLocation = new System.Drawing.Point(317, 203);
            this.bar17.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem1, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem2, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem3, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem4, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem5, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar17.OptionsBar.MultiLine = true;
            this.bar17.OptionsBar.UseWholeRow = true;
            this.bar17.Text = "Main menu";
            // 
            // qLDSVDataSet
            // 
            this.qLDSVDataSet.DataSetName = "QLDSVDataSet";
            this.qLDSVDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // v_DSPMTableAdapter
            // 
            this.v_DSPMTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Main menu";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(317, 203);
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Main menu";
            // 
            // colTEN
            // 
            this.colTEN.FieldName = "TEN";
            this.colTEN.Name = "colTEN";
            this.colTEN.Visible = true;
            this.colTEN.VisibleIndex = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(84, 90);
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // bar3
            // 
            this.bar3.BarName = "Main menu";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar3.FloatLocation = new System.Drawing.Point(84, 90);
            this.bar3.OptionsBar.MultiLine = true;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Main menu";
            // 
            // bar4
            // 
            this.bar4.BarName = "Main menu";
            this.bar4.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar4.DockCol = 0;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar4.FloatLocation = new System.Drawing.Point(84, 90);
            this.bar4.OptionsBar.MultiLine = true;
            this.bar4.OptionsBar.UseWholeRow = true;
            this.bar4.Text = "Main menu";
            // 
            // bar5
            // 
            this.bar5.BarName = "Main menu";
            this.bar5.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar5.DockCol = 0;
            this.bar5.DockRow = 0;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar5.FloatLocation = new System.Drawing.Point(84, 90);
            this.bar5.OptionsBar.MultiLine = true;
            this.bar5.OptionsBar.UseWholeRow = true;
            this.bar5.Text = "Main menu";
            // 
            // bar6
            // 
            this.bar6.BarName = "Main menu";
            this.bar6.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar6.DockCol = 0;
            this.bar6.DockRow = 0;
            this.bar6.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar6.FloatLocation = new System.Drawing.Point(84, 90);
            this.bar6.OptionsBar.MultiLine = true;
            this.bar6.OptionsBar.UseWholeRow = true;
            this.bar6.Text = "Main menu";
            // 
            // bar7
            // 
            this.bar7.BarName = "Main menu";
            this.bar7.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar7.DockCol = 0;
            this.bar7.DockRow = 0;
            this.bar7.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar7.FloatLocation = new System.Drawing.Point(84, 90);
            this.bar7.OptionsBar.MultiLine = true;
            this.bar7.OptionsBar.UseWholeRow = true;
            this.bar7.Text = "Main menu";
            // 
            // bar8
            // 
            this.bar8.BarName = "Main menu";
            this.bar8.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar8.DockCol = 0;
            this.bar8.DockRow = 0;
            this.bar8.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar8.FloatLocation = new System.Drawing.Point(84, 90);
            this.bar8.OptionsBar.MultiLine = true;
            this.bar8.OptionsBar.UseWholeRow = true;
            this.bar8.Text = "Main menu";
            // 
            // bar10
            // 
            this.bar10.BarName = "Main menu";
            this.bar10.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar10.DockCol = 0;
            this.bar10.DockRow = 0;
            this.bar10.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar10.FloatLocation = new System.Drawing.Point(84, 90);
            this.bar10.OptionsBar.MultiLine = true;
            this.bar10.OptionsBar.UseWholeRow = true;
            this.bar10.Text = "Main menu";
            // 
            // bar11
            // 
            this.bar11.BarName = "Main menu";
            this.bar11.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar11.DockCol = 0;
            this.bar11.DockRow = 0;
            this.bar11.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar11.FloatLocation = new System.Drawing.Point(84, 90);
            this.bar11.OptionsBar.MultiLine = true;
            this.bar11.OptionsBar.UseWholeRow = true;
            this.bar11.Text = "Main menu";
            // 
            // bar12
            // 
            this.bar12.BarName = "Main menu";
            this.bar12.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar12.DockCol = 0;
            this.bar12.DockRow = 0;
            this.bar12.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar12.FloatLocation = new System.Drawing.Point(84, 90);
            this.bar12.OptionsBar.MultiLine = true;
            this.bar12.OptionsBar.UseWholeRow = true;
            this.bar12.Text = "Main menu";
            // 
            // bar13
            // 
            this.bar13.BarName = "Main menu";
            this.bar13.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar13.DockCol = 0;
            this.bar13.DockRow = 0;
            this.bar13.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar13.FloatLocation = new System.Drawing.Point(84, 90);
            this.bar13.OptionsBar.MultiLine = true;
            this.bar13.OptionsBar.UseWholeRow = true;
            this.bar13.Text = "Main menu";
            // 
            // bar14
            // 
            this.bar14.BarName = "Main menu";
            this.bar14.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar14.DockCol = 0;
            this.bar14.DockRow = 0;
            this.bar14.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar14.FloatLocation = new System.Drawing.Point(84, 90);
            this.bar14.OptionsBar.MultiLine = true;
            this.bar14.OptionsBar.UseWholeRow = true;
            this.bar14.Text = "Main menu";
            // 
            // bar9
            // 
            this.bar9.BarName = "Main menu";
            this.bar9.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar9.DockCol = 0;
            this.bar9.DockRow = 0;
            this.bar9.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar9.FloatLocation = new System.Drawing.Point(84, 90);
            this.bar9.OptionsBar.MultiLine = true;
            this.bar9.OptionsBar.UseWholeRow = true;
            this.bar9.Text = "Main menu";
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.DIEMTableAdapter = null;
            this.tableAdapterManager.GIANGVIENTableAdapter = null;
            this.tableAdapterManager.HOCPHITableAdapter = null;
            this.tableAdapterManager.KHOATableAdapter = null;
            this.tableAdapterManager.LOPTableAdapter = null;
            this.tableAdapterManager.MONHOCTableAdapter = null;
            this.tableAdapterManager.SINHVIENTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.comboBoxKhoa);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 40);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(910, 46);
            this.panel1.TabIndex = 28;
            // 
            // comboBoxKhoa
            // 
            this.comboBoxKhoa.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBoxKhoa.DisplayMember = "subscriber_server";
            this.comboBoxKhoa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxKhoa.FormattingEnabled = true;
            this.comboBoxKhoa.Location = new System.Drawing.Point(102, 10);
            this.comboBoxKhoa.Name = "comboBoxKhoa";
            this.comboBoxKhoa.Size = new System.Drawing.Size(300, 21);
            this.comboBoxKhoa.TabIndex = 1;
            this.comboBoxKhoa.ValueMember = "subscriber_server";
            this.comboBoxKhoa.SelectedIndexChanged += new System.EventHandler(this.comboBoxKhoa_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "KHOA:";
            // 
            // panlFill
            // 
            this.panlFill.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panlFill.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panlFill.Controls.Add(this.btnBack);
            this.panlFill.Controls.Add(this.btnXong);
            this.panlFill.Controls.Add(this.grFill);
            this.panlFill.Dock = System.Windows.Forms.DockStyle.Top;
            this.panlFill.Location = new System.Drawing.Point(0, 86);
            this.panlFill.Name = "panlFill";
            this.panlFill.Size = new System.Drawing.Size(910, 75);
            this.panlFill.TabIndex = 31;
            // 
            // btnBack
            // 
            this.btnBack.Enabled = false;
            this.btnBack.Location = new System.Drawing.Point(812, 29);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 1;
            this.btnBack.Text = "TRỞ LẠI";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnXong
            // 
            this.btnXong.Enabled = false;
            this.btnXong.Location = new System.Drawing.Point(730, 29);
            this.btnXong.Name = "btnXong";
            this.btnXong.Size = new System.Drawing.Size(75, 23);
            this.btnXong.TabIndex = 1;
            this.btnXong.Text = "XONG";
            this.btnXong.UseVisualStyleBackColor = true;
            this.btnXong.Click += new System.EventHandler(this.btnXong_Click);
            // 
            // grFill
            // 
            this.grFill.Controls.Add(this.mONHOCComboBox);
            this.grFill.Controls.Add(this.lOPComboBox);
            this.grFill.Controls.Add(this.comboLanThi);
            this.grFill.Controls.Add(this.label4);
            this.grFill.Controls.Add(this.label3);
            this.grFill.Controls.Add(this.label2);
            this.grFill.Controls.Add(this.btnNhapDiem);
            this.grFill.Cursor = System.Windows.Forms.Cursors.Default;
            this.grFill.Location = new System.Drawing.Point(6, 5);
            this.grFill.Name = "grFill";
            this.grFill.Size = new System.Drawing.Size(715, 63);
            this.grFill.TabIndex = 0;
            this.grFill.TabStop = false;
            // 
            // mONHOCComboBox
            // 
            this.mONHOCComboBox.DataSource = this.mONHOCBindingSource;
            this.mONHOCComboBox.DisplayMember = "TENMH";
            this.mONHOCComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mONHOCComboBox.FormattingEnabled = true;
            this.mONHOCComboBox.Location = new System.Drawing.Point(50, 39);
            this.mONHOCComboBox.Name = "mONHOCComboBox";
            this.mONHOCComboBox.Size = new System.Drawing.Size(445, 21);
            this.mONHOCComboBox.TabIndex = 22;
            this.mONHOCComboBox.ValueMember = "MAMH";
            // 
            // mONHOCBindingSource
            // 
            this.mONHOCBindingSource.DataMember = "MONHOC";
            this.mONHOCBindingSource.DataSource = this.qLDSVDataSetMain;
            // 
            // lOPComboBox
            // 
            this.lOPComboBox.DataSource = this.lOPBindingSource;
            this.lOPComboBox.DisplayMember = "TENLOP";
            this.lOPComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lOPComboBox.FormattingEnabled = true;
            this.lOPComboBox.Location = new System.Drawing.Point(50, 10);
            this.lOPComboBox.Name = "lOPComboBox";
            this.lOPComboBox.Size = new System.Drawing.Size(445, 21);
            this.lOPComboBox.TabIndex = 23;
            this.lOPComboBox.ValueMember = "MALOP";
            // 
            // lOPBindingSource
            // 
            this.lOPBindingSource.DataMember = "LOP";
            this.lOPBindingSource.DataSource = this.qLDSVDataSetMain;
            // 
            // comboLanThi
            // 
            this.comboLanThi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboLanThi.FormattingEnabled = true;
            this.comboLanThi.Location = new System.Drawing.Point(559, 26);
            this.comboLanThi.Name = "comboLanThi";
            this.comboLanThi.Size = new System.Drawing.Size(43, 21);
            this.comboLanThi.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "LỚP:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "MÔN:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(510, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Lần thi:";
            // 
            // btnNhapDiem
            // 
            this.btnNhapDiem.Location = new System.Drawing.Point(631, 24);
            this.btnNhapDiem.Name = "btnNhapDiem";
            this.btnNhapDiem.Size = new System.Drawing.Size(75, 23);
            this.btnNhapDiem.TabIndex = 18;
            this.btnNhapDiem.Text = "BẮT ĐẦU";
            this.btnNhapDiem.UseVisualStyleBackColor = true;
            this.btnNhapDiem.Click += new System.EventHandler(this.btnNhapDiem_Click);
            // 
            // lOPTableAdapter
            // 
            this.lOPTableAdapter.ClearBeforeFill = true;
            // 
            // mONHOCTableAdapter
            // 
            this.mONHOCTableAdapter.ClearBeforeFill = true;
            // 
            // sP_SUADIEMMONHOCBindingSource
            // 
            this.sP_SUADIEMMONHOCBindingSource.DataMember = "SP_SUADIEMMONHOC";
            this.sP_SUADIEMMONHOCBindingSource.DataSource = this.qLDSVDataSetMain;
            // 
            // sP_SUADIEMMONHOCTableAdapter
            // 
            this.sP_SUADIEMMONHOCTableAdapter.ClearBeforeFill = true;
            // 
            // sP_NHAPDIEMSINHVIENBindingSource
            // 
            this.sP_NHAPDIEMSINHVIENBindingSource.DataMember = "SP_NHAPDIEMSINHVIEN";
            this.sP_NHAPDIEMSINHVIENBindingSource.DataSource = this.qLDSVDataSetMain;
            // 
            // sP_NHAPDIEMSINHVIENTableAdapter
            // 
            this.sP_NHAPDIEMSINHVIENTableAdapter.ClearBeforeFill = true;
            // 
            // sP_NHAPDIEMSINHVIENGridControl
            // 
            this.sP_NHAPDIEMSINHVIENGridControl.DataSource = this.sP_NHAPDIEMSINHVIENBindingSource;
            this.sP_NHAPDIEMSINHVIENGridControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.sP_NHAPDIEMSINHVIENGridControl.EmbeddedNavigator.Appearance.BackColor = System.Drawing.Color.White;
            this.sP_NHAPDIEMSINHVIENGridControl.EmbeddedNavigator.Appearance.Options.UseBackColor = true;
            this.sP_NHAPDIEMSINHVIENGridControl.Location = new System.Drawing.Point(0, 161);
            this.sP_NHAPDIEMSINHVIENGridControl.MainView = this.gridView1;
            this.sP_NHAPDIEMSINHVIENGridControl.MenuManager = this.barManager1;
            this.sP_NHAPDIEMSINHVIENGridControl.Name = "sP_NHAPDIEMSINHVIENGridControl";
            this.sP_NHAPDIEMSINHVIENGridControl.Size = new System.Drawing.Size(910, 150);
            this.sP_NHAPDIEMSINHVIENGridControl.TabIndex = 63;
            this.sP_NHAPDIEMSINHVIENGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.sP_NHAPDIEMSINHVIENGridControl.Visible = false;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.Blue;
            this.gridView1.Appearance.SelectedRow.BackColor2 = System.Drawing.Color.Blue;
            this.gridView1.Appearance.SelectedRow.BorderColor = System.Drawing.Color.Blue;
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMASV1,
            this.colHOTEN1,
            this.colDIEM1});
            this.gridView1.GridControl = this.sP_NHAPDIEMSINHVIENGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colMASV1
            // 
            this.colMASV1.Caption = "MÃ SV";
            this.colMASV1.FieldName = "MASV";
            this.colMASV1.Name = "colMASV1";
            this.colMASV1.OptionsColumn.AllowEdit = false;
            this.colMASV1.OptionsColumn.AllowFocus = false;
            this.colMASV1.Visible = true;
            this.colMASV1.VisibleIndex = 0;
            // 
            // colHOTEN1
            // 
            this.colHOTEN1.Caption = "HỌ TÊN";
            this.colHOTEN1.FieldName = "HOTEN";
            this.colHOTEN1.Name = "colHOTEN1";
            this.colHOTEN1.OptionsColumn.AllowEdit = false;
            this.colHOTEN1.OptionsColumn.AllowFocus = false;
            this.colHOTEN1.Visible = true;
            this.colHOTEN1.VisibleIndex = 1;
            // 
            // colDIEM1
            // 
            this.colDIEM1.Caption = "ĐIỂM";
            this.colDIEM1.FieldName = "DIEM";
            this.colDIEM1.Name = "colDIEM1";
            this.colDIEM1.Visible = true;
            this.colDIEM1.VisibleIndex = 2;
            // 
            // sP_SUADIEMMONHOCGridControl
            // 
            this.sP_SUADIEMMONHOCGridControl.DataSource = this.sP_SUADIEMMONHOCBindingSource;
            this.sP_SUADIEMMONHOCGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sP_SUADIEMMONHOCGridControl.Location = new System.Drawing.Point(0, 311);
            this.sP_SUADIEMMONHOCGridControl.MainView = this.gridView2;
            this.sP_SUADIEMMONHOCGridControl.MenuManager = this.barManager1;
            this.sP_SUADIEMMONHOCGridControl.Name = "sP_SUADIEMMONHOCGridControl";
            this.sP_SUADIEMMONHOCGridControl.Size = new System.Drawing.Size(910, 123);
            this.sP_SUADIEMMONHOCGridControl.TabIndex = 63;
            this.sP_SUADIEMMONHOCGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMASV,
            this.colHOTEN,
            this.colDIEM});
            this.gridView2.GridControl = this.sP_SUADIEMMONHOCGridControl;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // colMASV
            // 
            this.colMASV.Caption = "MÃ SV";
            this.colMASV.FieldName = "MASV";
            this.colMASV.Name = "colMASV";
            this.colMASV.OptionsColumn.AllowEdit = false;
            this.colMASV.OptionsColumn.AllowFocus = false;
            this.colMASV.Visible = true;
            this.colMASV.VisibleIndex = 0;
            // 
            // colHOTEN
            // 
            this.colHOTEN.Caption = "HỌ TÊN";
            this.colHOTEN.FieldName = "HOTEN";
            this.colHOTEN.Name = "colHOTEN";
            this.colHOTEN.OptionsColumn.AllowEdit = false;
            this.colHOTEN.OptionsColumn.AllowFocus = false;
            this.colHOTEN.Visible = true;
            this.colHOTEN.VisibleIndex = 1;
            // 
            // colDIEM
            // 
            this.colDIEM.Caption = "ĐIỂM";
            this.colDIEM.FieldName = "DIEM";
            this.colDIEM.Name = "colDIEM";
            this.colDIEM.Visible = true;
            this.colDIEM.VisibleIndex = 2;
            // 
            // FormDIEM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 457);
            this.Controls.Add(this.sP_SUADIEMMONHOCGridControl);
            this.Controls.Add(this.sP_NHAPDIEMSINHVIENGridControl);
            this.Controls.Add(this.panlFill);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "FormDIEM";
            this.Text = "Điểm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormDIEM_FormClosing);
            this.Load += new System.EventHandler(this.FormDIEM_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSetMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panlFill.ResumeLayout(false);
            this.grFill.ResumeLayout(false);
            this.grFill.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mONHOCBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lOPBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sP_SUADIEMMONHOCBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sP_NHAPDIEMSINHVIENBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sP_NHAPDIEMSINHVIENGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sP_SUADIEMMONHOCGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar15;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.Bar bar16;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private QLDSVDataSetMain qLDSVDataSetMain;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraBars.Bar bar17;
        private QLDSVDataSet qLDSVDataSet;
        private QLDSVDataSetTableAdapters.V_DSPMTableAdapter v_DSPMTableAdapter;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraGrid.Columns.GridColumn colTEN;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.Bar bar5;
        private DevExpress.XtraBars.Bar bar6;
        private DevExpress.XtraBars.Bar bar7;
        private DevExpress.XtraBars.Bar bar8;
        private DevExpress.XtraBars.Bar bar10;
        private DevExpress.XtraBars.Bar bar11;
        private DevExpress.XtraBars.Bar bar12;
        private DevExpress.XtraBars.Bar bar13;
        private DevExpress.XtraBars.Bar bar14;
        private DevExpress.XtraBars.Bar bar9;
        private QLDSVDataSetMainTableAdapters.TableAdapterManager tableAdapterManager;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBoxKhoa;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panlFill;
        private System.Windows.Forms.BindingSource lOPBindingSource;
        private QLDSVDataSetMainTableAdapters.LOPTableAdapter lOPTableAdapter;
        private System.Windows.Forms.BindingSource mONHOCBindingSource;
        private QLDSVDataSetMainTableAdapters.MONHOCTableAdapter mONHOCTableAdapter;
        private System.Windows.Forms.Button btnXong;
        private System.Windows.Forms.GroupBox grFill;
        private System.Windows.Forms.ComboBox mONHOCComboBox;
        private System.Windows.Forms.ComboBox lOPComboBox;
        private System.Windows.Forms.ComboBox comboLanThi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnNhapDiem;
        private DevExpress.XtraBars.BarButtonItem btnExit;
        private System.Windows.Forms.BindingSource sP_SUADIEMMONHOCBindingSource;
        private QLDSVDataSetMainTableAdapters.SP_SUADIEMMONHOCTableAdapter sP_SUADIEMMONHOCTableAdapter;
        private System.Windows.Forms.BindingSource sP_NHAPDIEMSINHVIENBindingSource;
        private QLDSVDataSetMainTableAdapters.SP_NHAPDIEMSINHVIENTableAdapter sP_NHAPDIEMSINHVIENTableAdapter;
        private DevExpress.XtraGrid.GridControl sP_SUADIEMMONHOCGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colMASV;
        private DevExpress.XtraGrid.Columns.GridColumn colHOTEN;
        private DevExpress.XtraGrid.Columns.GridColumn colDIEM;
        private DevExpress.XtraGrid.GridControl sP_NHAPDIEMSINHVIENGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colMASV1;
        private DevExpress.XtraGrid.Columns.GridColumn colHOTEN1;
        private DevExpress.XtraGrid.Columns.GridColumn colDIEM1;
        private System.Windows.Forms.Button btnBack;
    }
}