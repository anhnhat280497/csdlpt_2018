﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QLDSV_CSDLPT
{
    public partial class formMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public formMain()
        {
            InitializeComponent();
            KhoiTaoCacGiaTri();
        }
        private void KhoiTaoCacGiaTri()
        {
            
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            SqlConnection sqlConnection = new SqlConnection(Program.connectionString);
            sqlConnection.Open();
            string sql = "EXEC SP_DANGNHAP '" + Program.userName + "'";

            SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection);
            SqlDataReader dataReader = sqlCommand.ExecuteReader();
            dataReader.Read();
            labelMaGV.Text += dataReader.GetString(0);
            labelNameGV.Text += dataReader.GetString(1);
            labelGroupGV.Text += dataReader.GetString(2);
            Program.groupHT = dataReader.GetString(2);
            if (Program.groupHT == "PKeToan")
            {
                //btnBarHP.Enabled = barDongHPCuaLop.Enabled = true;
                btnBarDiem.Enabled = btnBarLop.Enabled = btnBarMH.Enabled = btnBarSV.Enabled
                    = btnBarReportDSSV.Enabled = barDSSVThiHetMon.Enabled = barPhieuDiemSV.Enabled
                    = barBangDiemMH.Enabled = false;
            }
            else btnBarHP.Enabled = barDongHPCuaLop.Enabled = false;
            
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.bindingSourceKhoa.RemoveFilter();
            Program.formLogin.Visible = true;
        }
        
        private void btnBarLop_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.formLOP == null)
            {
                Program.formLOP = new formLOP();
                Program.formLOP.MdiParent = this;
                Program.formLOP.Visible = true;
            }
            else Program.formLOP.Focus();
        }

        private void btnBarSV_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.sformSV == null)
            {
                Program.sformSV = new sformSV();
                Program.sformSV.MdiParent = this;
                Program.sformSV.Visible = true;
            }
            else Program.sformSV.Focus();
        }

        private void btnBarMH_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.formMH == null)
            {
                Program.formMH = new formMH();
                Program.formMH.MdiParent = this;
                Program.formMH.Visible = true;
            }
            else Program.formMH.Focus();
        }

        private void btnBarDiem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.formDiem == null)
            {
                Program.formDiem = new FormDIEM();
                Program.formDiem.MdiParent = this;
                Program.formDiem.Visible = true;
            }
            else Program.formDiem.Focus();
        }

        private void btnBarHP_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.formHocPhi == null)
            {
                Program.formHocPhi = new formHP();
                Program.formHocPhi.MdiParent = this;
                Program.formHocPhi.Visible = true;
            }
            else Program.formHocPhi.Focus();
        }

        private void btnBarReportDSSV_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.inDanhSachSinhVien == null)
            {
                Program.inDanhSachSinhVien = new BaoCao.XtraForm1();
                Program.inDanhSachSinhVien.MdiParent = this;
                Program.inDanhSachSinhVien.Visible = true;
            }
            else Program.inDanhSachSinhVien.Focus();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.dSThiHetMon == null)
            {
                Program.dSThiHetMon = new BaoCao.rpFormDSThiHetMon();
                Program.dSThiHetMon.MdiParent = this;
                Program.dSThiHetMon.Visible = true;
            }
            else Program.dSThiHetMon.Focus();
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.rpFormBDMH == null)
            {
                Program.rpFormBDMH = new BaoCao.rpFormBangDiemMH();
                Program.rpFormBDMH.MdiParent = this;
                Program.rpFormBDMH.Visible = true;
            }
            else Program.rpFormBDMH.Focus();
        }

        private void barButtonItem5_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.rpFormPDSV == null)
            {
                Program.rpFormPDSV = new BaoCao.rpFormPhieuDiemSV();
                Program.rpFormPDSV.MdiParent = this;
                Program.rpFormPDSV.Visible = true;
            }
            else Program.rpFormPDSV.Focus();
        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Program.rpFormDSHP == null)
            {
                Program.rpFormDSHP = new BaoCao.rpFormDSDongHP();
                Program.rpFormDSHP.MdiParent = this;
                Program.rpFormDSHP.Visible = true;
            }
            else Program.rpFormDSHP.Focus();
        }
    }
}
