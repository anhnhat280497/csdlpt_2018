﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Columns;

namespace QLDSV_CSDLPT
{
    public partial class FormDIEM : DevExpress.XtraEditors.XtraForm
    {
        Boolean nhapdiem = false;
        public FormDIEM()
        {
            InitializeComponent();
        }
 
 
        private void FormDIEM_Load(object sender, EventArgs e)
        {
            this.mONHOCTableAdapter.Connection.ConnectionString = Program.connectionString;
            this.mONHOCTableAdapter.Fill(this.qLDSVDataSetMain.MONHOC);

            this.lOPTableAdapter.Connection.ConnectionString = Program.connectionString;
            this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);

            this.comboBoxKhoa.DataSource = Program.bindingSourceKhoa;
            comboBoxKhoa.DisplayMember = "description";
            comboBoxKhoa.ValueMember = "subscriber_server";

            if (Program.groupHT == "USER" || Program.groupHT == "KHOA")
            {
                comboBoxKhoa.Enabled = false;
            }
            else
            {
                Program.bindingSourceKhoa.Filter = "description <> 'Quản lý học phí'";
            }

                // set value cho combobox Lần thi
            IDictionary<int, string> dict = new Dictionary<int, string>();
            dict.Add(1, "1");
            dict.Add(2, "2");
            comboLanThi.DataSource = new BindingSource(dict, null);
            comboLanThi.DisplayMember = "Value";
            comboLanThi.ValueMember = "Key";
        }

        private bool kiemtraDiem(string diem)
        {

            if (diem == "")
            {
                MessageBox.Show("Bạn chưa nhập điểm cho sinh viên này!");
                return false;
            }
            if (float.Parse(diem) > 10)
            {
                MessageBox.Show("Điểm không được lớn hơn 10!");
                return false;
            }
            if (float.Parse(diem) < 0)
            {
                MessageBox.Show("Điểm phải lớn hơn hoặc bằng 0!");
                return false;
            }
            return true;
        }

        private void btnXong_Click(object sender, EventArgs e)
        {
            if (this.nhapdiem)
            {
                sP_NHAPDIEMSINHVIENBindingSource.EndEdit();
                int dem = sP_NHAPDIEMSINHVIENBindingSource.Count;
                for (int i = 0; i < dem; i++) //kiểm tra hợp lệ dữ liệu nhập vào!
                {
                    if (!kiemtraDiem(((DataRowView)sP_NHAPDIEMSINHVIENBindingSource[i])["DIEM"].ToString()))
                    {
                        gridView1.FocusedRowHandle = i;
                        gridView1.FocusedColumn = gridView1.Columns["DIEM"];
                        gridView1.ShowEditor();
                        return;
                    }

                }
                string sql = "";
                for (int i = 0; i < dem; i++)
                {
                    sql = "INSERT INTO dbo.DIEM (MASV,MAMH,LAN,DIEM) VALUES (@MASV,@MAMH, @LAN, @DIEM)";
                    using (SqlConnection connection = new SqlConnection(Program.connectionString))
                    using (SqlCommand cm = new SqlCommand(sql, connection))
                    {
                        cm.Parameters.AddWithValue("@MASV", gridView1.GetRowCellValue(i, "MASV").ToString().Trim());
                        cm.Parameters.AddWithValue("@MAMH", mONHOCComboBox.SelectedValue);
                        cm.Parameters.AddWithValue("@LAN", comboLanThi.Text);
                        cm.Parameters.AddWithValue("@DIEM", float.Parse(gridView1.GetRowCellValue(i, "DIEM").ToString().Trim()));

                        connection.Open();
                        try
                        {
                            cm.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Lỗi lưu điểm!");
                            connection.Close();
                            return;
                        }
                        connection.Close();
                    }
                }
            }
            else
            {
                sP_SUADIEMMONHOCBindingSource.EndEdit();
                int dem = sP_SUADIEMMONHOCBindingSource.Count;
                for (int i = 0; i < dem; i++)
                {
                    if (!kiemtraDiem(((DataRowView)sP_SUADIEMMONHOCBindingSource[i])["DIEM"].ToString()))
                    {
                        gridView1.FocusedRowHandle = i;
                        gridView1.FocusedColumn = gridView1.Columns["DIEM"];
                        gridView1.ShowEditor();
                        return;
                    }
                    //else
                    //{
                    //    try
                    //    {
                    //        diem = Int16.Parse(gridView2.GetRowCellValue(i, "DIEM").ToString().Trim());
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        diem = -1;
                    //    }
                    //}
                    //if (diem == -1)
                    //{
                    //    MessageBox.Show("Điểm không hợp lệ!");
                    //    gridView2.FocusedRowHandle = i;
                    //    gridView2.FocusedColumn = gridView2.VisibleColumns[2];
                    //    gridView2.ShowEditor();
                    //    return;
                    //}
                }
                string sql = "";
                for (int i = 0; i < dem; i++)
                {
                    //sql = @"insert dbo.DIEM (MASV, MAMH, LAN, DIEM) values (@MASV, @MAMH, @LAN, @DIEM)";

                    //using (SqlConnection cn = new SqlConnection(Program.connectionString))
                    //using (SqlCommand cm = new SqlCommand(sql, cn))
                    //using (SqlCommand delete = new SqlCommand("delete from dbo.DIEM where MASV = @MASV and MAMH = @MAMH and LAN = @LAN",cn))
                    //{

                    //    cm.Parameters.AddWithValue("@MASV", sP_SUADIEMMONHOCDataGridView.Rows[i].Cells[0].Value.ToString().Trim());
                    //    cm.Parameters.AddWithValue("@MAMH", mONHOCComboBox.SelectedValue);
                    //    cm.Parameters.AddWithValue("@LAN", comboLanThi.Text);
                    //    cm.Parameters.AddWithValue("@DIEM", sP_SUADIEMMONHOCDataGridView.Rows[i].Cells[2].Value.ToString().Trim());

                    //    delete.Parameters.AddWithValue("@MASV", sP_SUADIEMMONHOCDataGridView.Rows[i].Cells[0].Value.ToString().Trim());
                    //    delete.Parameters.AddWithValue("@MAMH", mONHOCComboBox.SelectedValue);
                    //    delete.Parameters.AddWithValue("@LAN", comboLanThi.Text);

                    //    cn.Open();
                    //    delete.ExecuteNonQuery();
                    //    cm.ExecuteNonQuery();
                    //    cn.Close();
                    //}
                    sql = "UPDATE dbo.DIEM SET DIEM = @DIEM WHERE MASV = @MASV and MAMH = @MAMH and LAN = @LAN";

                    using (SqlConnection connection = new SqlConnection(Program.connectionString))
                    using (SqlCommand cm = new SqlCommand(sql, connection))
                    {
                        cm.Parameters.AddWithValue("@MASV", gridView2.GetRowCellValue(i, "MASV").ToString().Trim());
                        cm.Parameters.AddWithValue("@MAMH", mONHOCComboBox.SelectedValue);
                        cm.Parameters.AddWithValue("@LAN", comboLanThi.Text);
                        cm.Parameters.AddWithValue("@DIEM", float.Parse(gridView2.GetRowCellValue(i, "DIEM").ToString().Trim()));

                        connection.Open();
                        try
                        {
                            cm.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Lỗi lưu điểm!");
                            connection.Close();
                            return;
                        }
                        connection.Close();
                    }
                }
            }
            sP_NHAPDIEMSINHVIENTableAdapter.Fill(qLDSVDataSetMain.SP_NHAPDIEMSINHVIEN, "");
            sP_SUADIEMMONHOCTableAdapter.Fill(qLDSVDataSetMain.SP_SUADIEMMONHOC, "", "", 0);
            grFill.Enabled = panel1.Enabled = true;
            btnXong.Enabled = btnBack.Enabled = false;
        }

        private void btnNhapDiem_Click(object sender, EventArgs e)
        {
            string sql = "EXEC SP_SUADIEMMONHOC " + this.lOPComboBox.SelectedValue.ToString() + ","
                + this.mONHOCComboBox.SelectedValue.ToString() +","+this.comboLanThi.SelectedValue.ToString();
            using (SqlConnection connection = new SqlConnection(Program.connectionString))
            using (SqlCommand cm = new SqlCommand(sql, connection))
            {
                connection.Open();
                try
                {
                    SqlDataReader sqlDataReader = cm.ExecuteReader();
                    if (sqlDataReader.HasRows)
                    {
                        this.nhapdiem = false;
                        sP_NHAPDIEMSINHVIENGridControl.Visible = false;
                        sP_SUADIEMMONHOCGridControl.Visible = true;
                        sP_SUADIEMMONHOCGridControl.Dock = DockStyle.Fill;
                        sP_SUADIEMMONHOCTableAdapter.Connection.ConnectionString = Program.connectionString;
                        sP_SUADIEMMONHOCTableAdapter.Fill(qLDSVDataSetMain.SP_SUADIEMMONHOC, this.lOPComboBox.SelectedValue.ToString(),
                            this.mONHOCComboBox.SelectedValue.ToString(), Int32.Parse(this.comboLanThi.SelectedValue.ToString()));
                        sqlDataReader.Close();
                    }
                    else
                    {
                        this.nhapdiem = true;
                        sqlDataReader.Close();
                        sP_SUADIEMMONHOCGridControl.Visible = false;
                        sP_NHAPDIEMSINHVIENGridControl.Visible = true;
                        sP_NHAPDIEMSINHVIENGridControl.Dock = DockStyle.Fill;
                        sP_NHAPDIEMSINHVIENTableAdapter.Connection.ConnectionString = Program.connectionString;
                        try
                        {
                            sP_NHAPDIEMSINHVIENTableAdapter.Fill(qLDSVDataSetMain.SP_NHAPDIEMSINHVIEN,
                                lOPComboBox.SelectedValue.ToString().Trim());
                            if (sP_NHAPDIEMSINHVIENBindingSource.Count == 0)
                            {
                                MessageBox.Show("Lớp không có sinh viên!");
                                grFill.Enabled = comboBoxKhoa.Enabled = true;
                                btnXong.Enabled = false;
                                return;
                            }
                        }
                        catch (System.Exception ex)
                        {
                            System.Windows.Forms.MessageBox.Show(ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    connection.Close();
                    return;
                }
                connection.Close();
            }
            grFill.Enabled = panel1.Enabled = false;
            btnXong.Enabled = btnBack.Enabled
                = sP_NHAPDIEMSINHVIENGridControl.Enabled = sP_SUADIEMMONHOCGridControl.Enabled = true;
        }

        private void comboBoxKhoa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBoxKhoa.ValueMember != "" && Program.formLogin.Visible == false)
            {
                if (Program.server != this.comboBoxKhoa.SelectedValue.ToString())
                {
                    Program.server = this.comboBoxKhoa.SelectedValue.ToString();
                    if (Program.userNameHTai != Program.remoteLogin)
                    {
                        Program.userNameHTai = Program.remoteLogin;
                        Program.passwordHTai = Program.remotePassword;
                    }
                    else
                    {
                        Program.userNameHTai = Program.userName;
                        Program.passwordHTai = Program.password;
                    }
                    Program.connectionString = "Server = " + Program.server + "; Database = " + Program.database + "; User Id = "
                        + Program.userNameHTai + "; Password = " + Program.passwordHTai;
                    Program.sqlConnection = new SqlConnection(Program.connectionString);
                    Program.sqlConnection.Open();
                    this.lOPTableAdapter.Connection.ConnectionString = Program.connectionString;
                    this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
                }
            }
        }

        private void btnExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void FormDIEM_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.formDiem = null;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            grFill.Enabled = panel1.Enabled = true;
            btnXong.Enabled = btnBack.Enabled 
                = sP_NHAPDIEMSINHVIENGridControl.Enabled = sP_SUADIEMMONHOCGridControl.Enabled = false;
        }
    }
}
