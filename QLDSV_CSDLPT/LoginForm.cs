﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;

namespace QLDSV_CSDLPT
{
    public partial class LoginForm : DevExpress.XtraEditors.XtraForm
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qLDSVDataSet.V_DSPM' table. You can move, or remove it, as needed.
            this.v_DSPMTableAdapter.Fill(this.qLDSVDataSet.V_DSPM);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Program.server = Program.khoaDN = DSPMComboBox.SelectedValue.ToString();
            Program.userName = usernameTextBox.Text;
            Program.password = passwordTextBox.Text;

            Program.connectionString = "Server = "+ Program.server +"; Database = "+ Program.database +"; User Id = "
                + Program.userName +"; Password = " + Program.password;
            Program.sqlConnection = new SqlConnection(Program.connectionString);
            try
            {
                Program.bindingSourceKhoa = new BindingSource();
                Program.bindingSourceKhoa = this.v_DSPMBindingSource;
                Program.sqlConnection.Open();
                Program.formMain = new formMain();
                Program.formMain.Activate();
                Program.formMain.Show();
                this.Visible = false;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Kết nối thất bại!" + ex.Message);
            }
        }
    }
}