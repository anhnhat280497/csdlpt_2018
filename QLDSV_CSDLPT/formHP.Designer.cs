﻿namespace QLDSV_CSDLPT
{
    partial class formHP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label nIENKHOALabel;
            System.Windows.Forms.Label hOCPHILabel;
            System.Windows.Forms.Label sOTIENDADONGLabel;
            System.Windows.Forms.Label mASVLabel;
            System.Windows.Forms.Label hOTENLabel;
            System.Windows.Forms.Label mALOPLabel;
            System.Windows.Forms.Label hOCKYLabel1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formHP));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnInsert = new DevExpress.XtraBars.BarButtonItem();
            this.btnDelete = new DevExpress.XtraBars.BarButtonItem();
            this.btnEdit = new DevExpress.XtraBars.BarButtonItem();
            this.btnSave = new DevExpress.XtraBars.BarButtonItem();
            this.btnUndo = new DevExpress.XtraBars.BarButtonItem();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.btnExit = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.xtraUserControl1 = new DevExpress.XtraEditors.XtraUserControl();
            this.qLDSVDataSetMain = new QLDSV_CSDLPT.QLDSVDataSetMain();
            this.sP_DONGHOCPHIBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sP_DONGHOCPHITableAdapter = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.SP_DONGHOCPHITableAdapter();
            this.tableAdapterManager = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.TableAdapterManager();
            this.panel1 = new System.Windows.Forms.Panel();
            this.sINHVIENComboBox = new System.Windows.Forms.ComboBox();
            this.sINHVIENBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnFill = new System.Windows.Forms.Button();
            this.mALOPLabel1 = new System.Windows.Forms.Label();
            this.sP_SVDONGHOCPHIBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.hOTENLabel1 = new System.Windows.Forms.Label();
            this.sP_DONGHOCPHIGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNIENKHOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHOCKY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHOCPHI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSOTIENDADONG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.hOCKYSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.sOTIENDADONGTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.hOCPHITextEdit = new DevExpress.XtraEditors.TextEdit();
            this.nIENKHOATextEdit = new DevExpress.XtraEditors.TextEdit();
            this.btnHoanTat = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.sP_SVDONGHOCPHITableAdapter = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.SP_SVDONGHOCPHITableAdapter();
            this.sINHVIENTableAdapter = new QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.SINHVIENTableAdapter();
            nIENKHOALabel = new System.Windows.Forms.Label();
            hOCPHILabel = new System.Windows.Forms.Label();
            sOTIENDADONGLabel = new System.Windows.Forms.Label();
            mASVLabel = new System.Windows.Forms.Label();
            hOTENLabel = new System.Windows.Forms.Label();
            mALOPLabel = new System.Windows.Forms.Label();
            hOCKYLabel1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSetMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sP_DONGHOCPHIBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sINHVIENBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sP_SVDONGHOCPHIBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sP_DONGHOCPHIGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hOCKYSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sOTIENDADONGTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hOCPHITextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nIENKHOATextEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // nIENKHOALabel
            // 
            nIENKHOALabel.AutoSize = true;
            nIENKHOALabel.Location = new System.Drawing.Point(11, 19);
            nIENKHOALabel.Name = "nIENKHOALabel";
            nIENKHOALabel.Size = new System.Drawing.Size(66, 13);
            nIENKHOALabel.TabIndex = 10;
            nIENKHOALabel.Text = "NIÊN KHÓA:";
            // 
            // hOCPHILabel
            // 
            hOCPHILabel.AutoSize = true;
            hOCPHILabel.Location = new System.Drawing.Point(275, 19);
            hOCPHILabel.Name = "hOCPHILabel";
            hOCPHILabel.Size = new System.Drawing.Size(53, 13);
            hOCPHILabel.TabIndex = 12;
            hOCPHILabel.Text = "HỌC PHÍ:";
            // 
            // sOTIENDADONGLabel
            // 
            sOTIENDADONGLabel.AutoSize = true;
            sOTIENDADONGLabel.Location = new System.Drawing.Point(228, 60);
            sOTIENDADONGLabel.Name = "sOTIENDADONGLabel";
            sOTIENDADONGLabel.Size = new System.Drawing.Size(102, 13);
            sOTIENDADONGLabel.TabIndex = 13;
            sOTIENDADONGLabel.Text = "SỐ TIỀN ĐÃ ĐÓNG:";
            // 
            // mASVLabel
            // 
            mASVLabel.AutoSize = true;
            mASVLabel.Location = new System.Drawing.Point(16, 14);
            mASVLabel.Name = "mASVLabel";
            mASVLabel.Size = new System.Drawing.Size(41, 13);
            mASVLabel.TabIndex = 0;
            mASVLabel.Text = "MÃ SV:";
            // 
            // hOTENLabel
            // 
            hOTENLabel.AutoSize = true;
            hOTENLabel.Location = new System.Drawing.Point(371, 14);
            hOTENLabel.Name = "hOTENLabel";
            hOTENLabel.Size = new System.Drawing.Size(48, 13);
            hOTENLabel.TabIndex = 2;
            hOTENLabel.Text = "HỌ TÊN:";
            // 
            // mALOPLabel
            // 
            mALOPLabel.AutoSize = true;
            mALOPLabel.Location = new System.Drawing.Point(681, 14);
            mALOPLabel.Name = "mALOPLabel";
            mALOPLabel.Size = new System.Drawing.Size(48, 13);
            mALOPLabel.TabIndex = 4;
            mALOPLabel.Text = "MÃ LỚP:";
            // 
            // hOCKYLabel1
            // 
            hOCKYLabel1.AutoSize = true;
            hOCKYLabel1.Location = new System.Drawing.Point(29, 60);
            hOCKYLabel1.Name = "hOCKYLabel1";
            hOCKYLabel1.Size = new System.Drawing.Size(48, 13);
            hOCKYLabel1.TabIndex = 15;
            hOCKYLabel1.Text = "HỌC KỲ:";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnInsert,
            this.btnDelete,
            this.btnEdit,
            this.btnSave,
            this.btnUndo,
            this.btnRefresh,
            this.btnExit});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 8;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnInsert, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnSave, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnUndo, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnRefresh, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnInsert
            // 
            this.btnInsert.Caption = "Insert";
            this.btnInsert.Id = 0;
            this.btnInsert.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnInsert.ImageOptions.Image")));
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnInsert_ItemClick);
            // 
            // btnDelete
            // 
            this.btnDelete.Caption = "Delete";
            this.btnDelete.Id = 1;
            this.btnDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.ImageOptions.Image")));
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDelete_ItemClick);
            // 
            // btnEdit
            // 
            this.btnEdit.Caption = "Edit";
            this.btnEdit.Id = 2;
            this.btnEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.ImageOptions.Image")));
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEdit_ItemClick);
            // 
            // btnSave
            // 
            this.btnSave.Caption = "Save";
            this.btnSave.Enabled = false;
            this.btnSave.Id = 4;
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.Name = "btnSave";
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSave_ItemClick);
            // 
            // btnUndo
            // 
            this.btnUndo.Caption = "Undo";
            this.btnUndo.Enabled = false;
            this.btnUndo.Id = 5;
            this.btnUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnUndo.ImageOptions.Image")));
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnUndo_ItemClick);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "Refresh";
            this.btnRefresh.Id = 6;
            this.btnRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.ImageOptions.Image")));
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
            // 
            // btnExit
            // 
            this.btnExit.Caption = "Exit";
            this.btnExit.Id = 7;
            this.btnExit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.ImageOptions.Image")));
            this.btnExit.Name = "btnExit";
            this.btnExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExit_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(883, 40);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 411);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(883, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 40);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 371);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(883, 40);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 371);
            // 
            // xtraUserControl1
            // 
            this.xtraUserControl1.Location = new System.Drawing.Point(46, 89);
            this.xtraUserControl1.Name = "xtraUserControl1";
            this.xtraUserControl1.Size = new System.Drawing.Size(8, 8);
            this.xtraUserControl1.TabIndex = 4;
            // 
            // qLDSVDataSetMain
            // 
            this.qLDSVDataSetMain.DataSetName = "QLDSVDataSetMain";
            this.qLDSVDataSetMain.EnforceConstraints = false;
            this.qLDSVDataSetMain.Locale = new System.Globalization.CultureInfo("en-US");
            this.qLDSVDataSetMain.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sP_DONGHOCPHIBindingSource
            // 
            this.sP_DONGHOCPHIBindingSource.DataMember = "SP_DONGHOCPHI";
            this.sP_DONGHOCPHIBindingSource.DataSource = this.qLDSVDataSetMain;
            // 
            // sP_DONGHOCPHITableAdapter
            // 
            this.sP_DONGHOCPHITableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.DIEMTableAdapter = null;
            this.tableAdapterManager.GIANGVIENTableAdapter = null;
            this.tableAdapterManager.HOCPHITableAdapter = null;
            this.tableAdapterManager.KHOATableAdapter = null;
            this.tableAdapterManager.LOPTableAdapter = null;
            this.tableAdapterManager.MONHOCTableAdapter = null;
            this.tableAdapterManager.SINHVIENTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = QLDSV_CSDLPT.QLDSVDataSetMainTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.sINHVIENComboBox);
            this.panel1.Controls.Add(this.btnFill);
            this.panel1.Controls.Add(mALOPLabel);
            this.panel1.Controls.Add(this.mALOPLabel1);
            this.panel1.Controls.Add(hOTENLabel);
            this.panel1.Controls.Add(this.hOTENLabel1);
            this.panel1.Controls.Add(mASVLabel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 40);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(883, 43);
            this.panel1.TabIndex = 7;
            // 
            // sINHVIENComboBox
            // 
            this.sINHVIENComboBox.DataSource = this.sINHVIENBindingSource;
            this.sINHVIENComboBox.DisplayMember = "MASV";
            this.sINHVIENComboBox.FormattingEnabled = true;
            this.sINHVIENComboBox.Location = new System.Drawing.Point(60, 11);
            this.sINHVIENComboBox.Name = "sINHVIENComboBox";
            this.sINHVIENComboBox.Size = new System.Drawing.Size(149, 21);
            this.sINHVIENComboBox.TabIndex = 6;
            this.sINHVIENComboBox.ValueMember = "MASV";
            // 
            // sINHVIENBindingSource
            // 
            this.sINHVIENBindingSource.DataMember = "SINHVIEN";
            this.sINHVIENBindingSource.DataSource = this.qLDSVDataSetMain;
            // 
            // btnFill
            // 
            this.btnFill.Location = new System.Drawing.Point(215, 11);
            this.btnFill.Name = "btnFill";
            this.btnFill.Size = new System.Drawing.Size(75, 23);
            this.btnFill.TabIndex = 6;
            this.btnFill.Text = "OK";
            this.btnFill.UseVisualStyleBackColor = true;
            this.btnFill.Click += new System.EventHandler(this.btnFill_Click);
            // 
            // mALOPLabel1
            // 
            this.mALOPLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sP_SVDONGHOCPHIBindingSource, "MALOP", true));
            this.mALOPLabel1.Location = new System.Drawing.Point(732, 9);
            this.mALOPLabel1.Name = "mALOPLabel1";
            this.mALOPLabel1.Size = new System.Drawing.Size(100, 23);
            this.mALOPLabel1.TabIndex = 5;
            // 
            // sP_SVDONGHOCPHIBindingSource
            // 
            this.sP_SVDONGHOCPHIBindingSource.DataMember = "SP_SVDONGHOCPHI";
            this.sP_SVDONGHOCPHIBindingSource.DataSource = this.qLDSVDataSetMain;
            // 
            // hOTENLabel1
            // 
            this.hOTENLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sP_SVDONGHOCPHIBindingSource, "HOTEN", true));
            this.hOTENLabel1.Location = new System.Drawing.Point(415, 9);
            this.hOTENLabel1.Name = "hOTENLabel1";
            this.hOTENLabel1.Size = new System.Drawing.Size(248, 23);
            this.hOTENLabel1.TabIndex = 3;
            // 
            // sP_DONGHOCPHIGridControl
            // 
            this.sP_DONGHOCPHIGridControl.DataSource = this.sP_DONGHOCPHIBindingSource;
            this.sP_DONGHOCPHIGridControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.sP_DONGHOCPHIGridControl.Location = new System.Drawing.Point(0, 83);
            this.sP_DONGHOCPHIGridControl.MainView = this.gridView1;
            this.sP_DONGHOCPHIGridControl.MenuManager = this.barManager1;
            this.sP_DONGHOCPHIGridControl.Name = "sP_DONGHOCPHIGridControl";
            this.sP_DONGHOCPHIGridControl.Size = new System.Drawing.Size(883, 220);
            this.sP_DONGHOCPHIGridControl.TabIndex = 23;
            this.sP_DONGHOCPHIGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNIENKHOA,
            this.colHOCKY,
            this.colHOCPHI,
            this.colSOTIENDADONG});
            this.gridView1.GridControl = this.sP_DONGHOCPHIGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colNIENKHOA
            // 
            this.colNIENKHOA.Caption = "NIÊN KHÓA";
            this.colNIENKHOA.FieldName = "NIENKHOA";
            this.colNIENKHOA.Name = "colNIENKHOA";
            this.colNIENKHOA.Visible = true;
            this.colNIENKHOA.VisibleIndex = 0;
            // 
            // colHOCKY
            // 
            this.colHOCKY.Caption = "HỌC KỲ";
            this.colHOCKY.FieldName = "HOCKY";
            this.colHOCKY.Name = "colHOCKY";
            this.colHOCKY.Visible = true;
            this.colHOCKY.VisibleIndex = 1;
            // 
            // colHOCPHI
            // 
            this.colHOCPHI.Caption = "HỌC PHÍ";
            this.colHOCPHI.FieldName = "HOCPHI";
            this.colHOCPHI.Name = "colHOCPHI";
            this.colHOCPHI.Visible = true;
            this.colHOCPHI.VisibleIndex = 2;
            // 
            // colSOTIENDADONG
            // 
            this.colSOTIENDADONG.Caption = "SỐ TIỀN ĐÃ ĐÓNG";
            this.colSOTIENDADONG.FieldName = "SOTIENDADONG";
            this.colSOTIENDADONG.Name = "colSOTIENDADONG";
            this.colSOTIENDADONG.Visible = true;
            this.colSOTIENDADONG.VisibleIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(hOCKYLabel1);
            this.panel2.Controls.Add(this.hOCKYSpinEdit);
            this.panel2.Controls.Add(sOTIENDADONGLabel);
            this.panel2.Controls.Add(this.sOTIENDADONGTextEdit);
            this.panel2.Controls.Add(hOCPHILabel);
            this.panel2.Controls.Add(this.hOCPHITextEdit);
            this.panel2.Controls.Add(nIENKHOALabel);
            this.panel2.Controls.Add(this.nIENKHOATextEdit);
            this.panel2.Controls.Add(this.btnHoanTat);
            this.panel2.Controls.Add(this.btnOK);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Enabled = false;
            this.panel2.Location = new System.Drawing.Point(0, 303);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(883, 108);
            this.panel2.TabIndex = 24;
            // 
            // hOCKYSpinEdit
            // 
            this.hOCKYSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sP_DONGHOCPHIBindingSource, "HOCKY", true));
            this.hOCKYSpinEdit.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.hOCKYSpinEdit.Location = new System.Drawing.Point(80, 55);
            this.hOCKYSpinEdit.MenuManager = this.barManager1;
            this.hOCKYSpinEdit.Name = "hOCKYSpinEdit";
            this.hOCKYSpinEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hOCKYSpinEdit.Properties.Appearance.Options.UseFont = true;
            this.hOCKYSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.hOCKYSpinEdit.Properties.IsFloatValue = false;
            this.hOCKYSpinEdit.Properties.Mask.EditMask = "N00";
            this.hOCKYSpinEdit.Properties.MaxValue = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.hOCKYSpinEdit.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.hOCKYSpinEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.hOCKYSpinEdit.Size = new System.Drawing.Size(38, 22);
            this.hOCKYSpinEdit.TabIndex = 16;
            // 
            // sOTIENDADONGTextEdit
            // 
            this.sOTIENDADONGTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sP_DONGHOCPHIBindingSource, "SOTIENDADONG", true));
            this.sOTIENDADONGTextEdit.Location = new System.Drawing.Point(331, 57);
            this.sOTIENDADONGTextEdit.MenuManager = this.barManager1;
            this.sOTIENDADONGTextEdit.Name = "sOTIENDADONGTextEdit";
            this.sOTIENDADONGTextEdit.Size = new System.Drawing.Size(175, 20);
            this.sOTIENDADONGTextEdit.TabIndex = 14;
            // 
            // hOCPHITextEdit
            // 
            this.hOCPHITextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sP_DONGHOCPHIBindingSource, "HOCPHI", true));
            this.hOCPHITextEdit.Location = new System.Drawing.Point(331, 16);
            this.hOCPHITextEdit.MenuManager = this.barManager1;
            this.hOCPHITextEdit.Name = "hOCPHITextEdit";
            this.hOCPHITextEdit.Size = new System.Drawing.Size(175, 20);
            this.hOCPHITextEdit.TabIndex = 13;
            // 
            // nIENKHOATextEdit
            // 
            this.nIENKHOATextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sP_DONGHOCPHIBindingSource, "NIENKHOA", true));
            this.nIENKHOATextEdit.Location = new System.Drawing.Point(80, 16);
            this.nIENKHOATextEdit.MenuManager = this.barManager1;
            this.nIENKHOATextEdit.Name = "nIENKHOATextEdit";
            this.nIENKHOATextEdit.Size = new System.Drawing.Size(149, 20);
            this.nIENKHOATextEdit.TabIndex = 11;
            // 
            // btnHoanTat
            // 
            this.btnHoanTat.Location = new System.Drawing.Point(683, 34);
            this.btnHoanTat.Name = "btnHoanTat";
            this.btnHoanTat.Size = new System.Drawing.Size(75, 23);
            this.btnHoanTat.TabIndex = 10;
            this.btnHoanTat.Text = "HOÀN TẤT";
            this.btnHoanTat.UseVisualStyleBackColor = true;
            this.btnHoanTat.Click += new System.EventHandler(this.btnHoanTat_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(586, 34);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 10;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // sP_SVDONGHOCPHITableAdapter
            // 
            this.sP_SVDONGHOCPHITableAdapter.ClearBeforeFill = true;
            // 
            // sINHVIENTableAdapter
            // 
            this.sINHVIENTableAdapter.ClearBeforeFill = true;
            // 
            // formHP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 434);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.sP_DONGHOCPHIGridControl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.xtraUserControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "formHP";
            this.Text = "formHP";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formHP_FormClosing);
            this.Load += new System.EventHandler(this.formHP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLDSVDataSetMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sP_DONGHOCPHIBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sINHVIENBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sP_SVDONGHOCPHIBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sP_DONGHOCPHIGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hOCKYSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sOTIENDADONGTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hOCPHITextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nIENKHOATextEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnInsert;
        private DevExpress.XtraBars.BarButtonItem btnDelete;
        private DevExpress.XtraBars.BarButtonItem btnEdit;
        private DevExpress.XtraBars.BarButtonItem btnSave;
        private DevExpress.XtraBars.BarButtonItem btnUndo;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        private DevExpress.XtraBars.BarButtonItem btnExit;
        private DevExpress.XtraEditors.XtraUserControl xtraUserControl1;
        private System.Windows.Forms.BindingSource sP_DONGHOCPHIBindingSource;
        private QLDSVDataSetMain qLDSVDataSetMain;
        private QLDSVDataSetMainTableAdapters.SP_DONGHOCPHITableAdapter sP_DONGHOCPHITableAdapter;
        private QLDSVDataSetMainTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.TextEdit sOTIENDADONGTextEdit;
        private DevExpress.XtraEditors.TextEdit hOCPHITextEdit;
        private DevExpress.XtraEditors.TextEdit nIENKHOATextEdit;
        private System.Windows.Forms.Button btnHoanTat;
        private System.Windows.Forms.Button btnOK;
        private DevExpress.XtraGrid.GridControl sP_DONGHOCPHIGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colNIENKHOA;
        private DevExpress.XtraGrid.Columns.GridColumn colHOCKY;
        private DevExpress.XtraGrid.Columns.GridColumn colHOCPHI;
        private DevExpress.XtraGrid.Columns.GridColumn colSOTIENDADONG;
        private System.Windows.Forms.BindingSource sP_SVDONGHOCPHIBindingSource;
        private QLDSVDataSetMainTableAdapters.SP_SVDONGHOCPHITableAdapter sP_SVDONGHOCPHITableAdapter;
        private System.Windows.Forms.Label mALOPLabel1;
        private System.Windows.Forms.Label hOTENLabel1;
        private System.Windows.Forms.Button btnFill;
        private System.Windows.Forms.BindingSource sINHVIENBindingSource;
        private QLDSVDataSetMainTableAdapters.SINHVIENTableAdapter sINHVIENTableAdapter;
        private System.Windows.Forms.ComboBox sINHVIENComboBox;
        private DevExpress.XtraEditors.SpinEdit hOCKYSpinEdit;
    }
}