﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLDSV_CSDLPT
{
    public partial class formLOP : Form
    {
        Boolean edit = false;
        public formLOP()
        {
            InitializeComponent();
        }

        private void FormLOP_Load(object sender, EventArgs e)
        {
            this.sINHVIENTableAdapter.Connection.ConnectionString = Program.connectionString;
            this.sINHVIENTableAdapter.Fill(this.qLDSVDataSetMain.SINHVIEN);

            this.lOPTableAdapter.Connection.ConnectionString = Program.connectionString;
            this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);

            this.comboBoxKhoa.DataSource = Program.bindingSourceKhoa;
            comboBoxKhoa.DisplayMember = "description";
            comboBoxKhoa.ValueMember = "subscriber_server";
            if (Program.groupHT == "USER")
            { // quyền USER không cho thêm xóa sửa LỚP
                comboBoxKhoa.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnInsert.Enabled = false;
            }
            else
            {
                Program.bindingSourceKhoa.Filter = "description <> 'Quản lý học phí'";
                if (comboBoxKhoa.SelectedValue.ToString() != Program.khoaDN && Program.groupHT == "KHOA")
                {
                    btnInsert.Enabled = btnSave.Enabled = btnDelete.Enabled = btnEdit.Enabled = false;
                }
                else
                {
                    btnInsert.Enabled = btnSave.Enabled = btnDelete.Enabled = btnEdit.Enabled = true;
                }
            }

            if (lOPBindingSource.Count == 0) btnDelete.Enabled = false;
            
        }

        private void btnInsert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.lOPBindingSource.AddNew();
            if (comboBoxKhoa.SelectedIndex == 0) mAKHComboBox.Text = "CNTT";
            else mAKHComboBox.Text = "VT";

            this.lOPGridControl.Enabled = this.comboBoxKhoa.Enabled = btnDelete.Enabled = btnEdit.Enabled =
                btnInsert.Enabled = btnRefresh.Enabled =  btnSave.Enabled = false;
            groupBox1.Enabled = btnHoanTat.Enabled = btnUndo.Enabled = true;
        }

        private void btnRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi Refresh :" + ex.Message, "", MessageBoxButtons.OK);
                return;
            }
            
        }

        private void btnSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                this.lOPBindingSource.EndEdit();
                this.lOPBindingSource.ResetCurrentItem();
                this.lOPTableAdapter.Update(this.qLDSVDataSetMain.LOP);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi lưu lớp.\n" + ex.Message, "", MessageBoxButtons.OK);
                return;
            }
            this.lOPGridControl.Enabled = this.comboBoxKhoa.Enabled = btnDelete.Enabled = btnEdit.Enabled =
                btnInsert.Enabled = btnRefresh.Enabled = true;
            btnSave.Enabled = btnUndo.Enabled = groupBox1.Enabled = edit = false;
        }

        
        private void comboBoxKhoa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBoxKhoa.ValueMember != "" && Program.formLogin.Visible == false)
            {
                if (Program.server != this.comboBoxKhoa.SelectedValue.ToString())
                {
                    Program.server = this.comboBoxKhoa.SelectedValue.ToString();
                    if (Program.userNameHTai != Program.remoteLogin)
                    {
                        Program.userNameHTai = Program.remoteLogin;
                        Program.passwordHTai = Program.remotePassword;
                    }
                    else
                    {
                        Program.userNameHTai = Program.userName;
                        Program.passwordHTai = Program.password;
                    }
                    Program.connectionString = "Server = " + Program.server + "; Database = " + Program.database + "; User Id = "
                        + Program.userNameHTai + "; Password = " + Program.passwordHTai;
                    Program.sqlConnection = new SqlConnection(Program.connectionString);
                    Program.sqlConnection.Open();
                    this.sINHVIENTableAdapter.Connection.ConnectionString = Program.connectionString;
                    this.sINHVIENTableAdapter.Fill(this.qLDSVDataSetMain.SINHVIEN);
                    this.lOPTableAdapter.Connection.ConnectionString = Program.connectionString;
                    this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
                    if(comboBoxKhoa.SelectedValue.ToString() != Program.khoaDN && Program.groupHT == "KHOA")
                    {
                        btnInsert.Enabled = btnSave.Enabled = btnDelete.Enabled = btnEdit.Enabled = false;
                    }
                    else
                    {
                        btnInsert.Enabled = btnSave.Enabled = btnDelete.Enabled = btnEdit.Enabled = true;
                    }
                }
            }
        }

        private void btnUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.lOPBindingSource.CancelEdit();
            this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
            btnRefresh.Enabled = btnEdit.Enabled = btnInsert.Enabled = btnDelete.Enabled =
                this.lOPGridControl.Enabled = this.comboBoxKhoa.Enabled = true;
            groupBox1.Enabled = btnSave.Enabled = btnUndo.Enabled = false;
        }

        private void btnDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            String maLop = "";
            if (sINHVIENBindingSource.Count > 0)
            {
                MessageBox.Show("Không thể xóa lớp này vì đã có sinh viên ", "",
                       MessageBoxButtons.OK);
                return;
            }
            if (MessageBox.Show("Bạn có thật sự muốn xóa lớp này ?? ", "Xác nhận",
                       MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    maLop = ((DataRowView)lOPBindingSource[lOPBindingSource.Position])["MALOP"].ToString(); // giữ lại để khi xóa bij lỗi thì ta sẽ quay về lại
                    lOPBindingSource.RemoveCurrent();
                    this.lOPTableAdapter.Connection.ConnectionString = Program.connectionString;
                    this.lOPTableAdapter.Update(this.qLDSVDataSetMain.LOP);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Lỗi xóa lớp. Bạn hãy xóa lại\n" + ex.Message, "",
                        MessageBoxButtons.OK);
                    this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
                    lOPBindingSource.Position = lOPBindingSource.Find("MALOP", maLop);
                    return;
                }
            }

            if (lOPBindingSource.Count == 0) btnDelete.Enabled = false;
        }

        private void btnEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btnDelete.Enabled = btnEdit.Enabled = btnInsert.Enabled = btnRefresh.Enabled =
                lOPGridControl.Enabled = comboBoxKhoa.Enabled = btnHoanTat.Enabled = false;
            groupBox1.Enabled = btnUndo.Enabled = edit = true;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (mALOPTextBox.Text.Trim() == "")
            {
                MessageBox.Show("Mã lớp không được trống!", "", MessageBoxButtons.OK);
                mALOPTextBox.Focus();
                return;
            }
            if (tENLOPTextBox.Text.Trim() == "")
            {
                MessageBox.Show("Tên lớp không được trống!", "", MessageBoxButtons.OK);
                tENLOPTextBox.Focus();
                return;
            }
            string maLOP = mALOPTextBox.Text, tenLOP = tENLOPTextBox.Text;
            int demtrung = 0;
            for (int i = 0; i < lOPBindingSource.Count; i++)
            {
                if (maLOP == ((DataRowView)lOPBindingSource[i])["MALOP"].ToString().Trim())
                {
                    demtrung++;
                }
            }
            if (demtrung == 2)
            {
                MessageBox.Show("Mã lớp bị trùng!");
                mALOPTextBox.Focus();
                return;
            }
            demtrung = 0;
            for (int i = 0; i < lOPBindingSource.Count; i++)
            {
                if (tenLOP == ((DataRowView)lOPBindingSource[i])["TENLOP"].ToString().Trim())
                {
                    demtrung++;
                }
            }
            if (demtrung == 2)
            {
                MessageBox.Show("Tên lớp bị trùng!");
                tENLOPTextBox.Focus();
                return;
            }

            string sql = "EXEC SP_TIMLOPTRENTOANSERVER " + maLOP;
            SqlCommand sqlCommand = new SqlCommand(sql, Program.sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            if (sqlDataReader.HasRows)
            {
                MessageBox.Show("Mã lớp bị trùng ở khoa khác!");
                sqlDataReader.Close();
                return;
            }
            sqlDataReader.Close();
            sql = "EXEC SP_TIMTENLOPTRENTOANSERVER N'" + tenLOP +"'";
            sqlCommand = new SqlCommand(sql, Program.sqlConnection);
            try
            {
                sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    MessageBox.Show("Tên lớp bị trùng ở khoa khác!");
                    sqlDataReader.Close();
                    return;
                }
                sqlDataReader.Close();
            } catch(Exception ex) { };
            

            this.lOPBindingSource.EndEdit();
            this.lOPBindingSource.ResetCurrentItem();
            if (!edit) this.lOPBindingSource.AddNew();
            else
            {
                btnSave.Enabled = true;
                btnUndo.Enabled = groupBox1.Enabled = false;
            }
        }

        private void btnHoanTat_Click(object sender, EventArgs e)
        {
            this.lOPBindingSource.RemoveAt(lOPBindingSource.Count - 1);
            this.lOPBindingSource.ResetCurrentItem();
            btnSave.Enabled =  true;
            groupBox1.Enabled = btnUndo.Enabled = false;
        }

        private void btnExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void formLOP_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.formLOP = null;
        }
    }
}
