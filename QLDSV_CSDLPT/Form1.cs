﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLDSV_CSDLPT
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void fillToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.sP_NHAPDIEMSINHVIENTableAdapter.Fill(this.qLDSVDataSetMain.SP_NHAPDIEMSINHVIEN, mALOPToolStripTextBox.Text);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }
        private bool kiemtrahople()
        {
            if (((DataRowView)sP_NHAPDIEMSINHVIENBindingSource.Current)["DIEM"].ToString() == "")
            {
                MessageBox.Show("Khong duoc rong");
                return false;
            }
            if (float.Parse(((DataRowView)sP_NHAPDIEMSINHVIENBindingSource.Current)["DIEM"].ToString()) > 10)
            {
                MessageBox.Show("Khong duoc > 10");
                return false;
            }
            return true;
        }

        private void gridView1_BeforeLeaveRow(object sender, DevExpress.XtraGrid.Views.Base.RowAllowEventArgs e)
        {
            if (!kiemtrahople())
            {
                e.Allow = false;
            }
        }
    }
}
