﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;

namespace QLDSV_CSDLPT
{
    public partial class sformSV : DevExpress.XtraEditors.XtraForm
    {
        Boolean edit = false, okInsert = false, khoaKhac = false;

        public sformSV()
        {
            InitializeComponent();
        }

        private void sformSV_Load(object sender, EventArgs e)
        {
            this.dIEMTableAdapter.Connection.ConnectionString = Program.connectionString;
            this.dIEMTableAdapter.Fill(this.qLDSVDataSetMain.DIEM);

            this.sINHVIENTableAdapter.Connection.ConnectionString = Program.connectionString;
            this.sINHVIENTableAdapter.Fill(this.qLDSVDataSetMain.SINHVIEN);

            this.lOPTableAdapter.Connection.ConnectionString = Program.connectionString;
            this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);

            this.comboBoxKhoa.DataSource = Program.bindingSourceKhoa;
            comboBoxKhoa.DisplayMember = "description";
            comboBoxKhoa.ValueMember = "subscriber_server";

            if (Program.groupHT == "USER")
            {
                comboBoxKhoa.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnInsert.Enabled = false;
            }
            else
            {
                Program.bindingSourceKhoa.Filter = "description <> 'Quản lý học phí'";
                if (comboBoxKhoa.SelectedValue.ToString() != Program.khoaDN && Program.groupHT == "KHOA")
                {
                    btnInsert.Enabled = btnSave.Enabled = btnDelete.Enabled = btnEdit.Enabled = khoaKhac = false;
                }
                else
                {
                    btnInsert.Enabled = btnDelete.Enabled = btnEdit.Enabled = khoaKhac = true;
                }
            }
            
        }

        private void btnInsert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.sINHVIENBindingSource.AddNew();
            mALOPTextBox.Text = ((DataRowView)lOPBindingSource[lOPBindingSource.Position])["MALOP"].ToString();
            pHAICheckEdit.Checked = true;
            nGHIHOCCheckEdit.Checked = okInsert = false;
            lOPGridControl.Enabled = this.comboBoxKhoa.Enabled = btnDelete.Enabled = btnEdit.Enabled =
                sINHVIENGridControl.Enabled=btnInsert.Enabled = btnRefresh.Enabled = btnSave.Enabled = edit = false;
            btnUndo.Enabled = btnHoanTat.Enabled = panelTTSV.Enabled = grBoxTTSV.Enabled = true;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (mASVTextEdit.Text.Trim() == "")
            {
                MessageBox.Show("Mã sinh viên không được trống!", "", MessageBoxButtons.OK);
                mASVTextEdit.Focus();
                return;
            }
            if (hOTextEdit.Text.Trim() == "")
            {
                MessageBox.Show("Họ không được trống!", "", MessageBoxButtons.OK);
                hOTextEdit.Focus();
                return;
            }
            if (tENTextEdit.Text.Trim() == "")
            {
                MessageBox.Show("Tên không được trống!", "", MessageBoxButtons.OK);
                tENTextEdit.Focus();
                return;
            }
            string maSV = mASVTextEdit.Text;
            int demtrung = 0;
            for (int i = 0; i < sINHVIENBindingSource.Count; i++)
            {
                if (maSV == ((DataRowView)sINHVIENBindingSource[i])["MASV"].ToString().Trim())
                {
                    demtrung++;
                }
            }
            if (demtrung == 2)
            {
                MessageBox.Show("Mã sinh viên bị trùng!");
                mASVTextEdit.Focus();
                return;
            }
            string sql = "EXEC SP_TIMSINHVIENTRENTOANSERVER " + maSV;
            SqlCommand sqlCommand = new SqlCommand(sql, Program.sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            if (sqlDataReader.HasRows)
            {
                MessageBox.Show("Mã sinh viên bị trùng ở khoa khác!");
                sqlDataReader.Close();
                return;
            }
            sqlDataReader.Close();
            this.sINHVIENBindingSource.EndEdit();
            this.sINHVIENBindingSource.ResetCurrentItem();
            btnUndo.Enabled = false;
            if (!edit)
            {
                this.sINHVIENBindingSource.AddNew();
                pHAICheckEdit.Checked = okInsert = true;
                nGHIHOCCheckEdit.Checked  = false;
            }
            else
            {
                btnSave.Enabled = true;
                panelTTSV.Enabled = false;
            }
            mALOPTextBox.Text = ((DataRowView)lOPBindingSource[lOPBindingSource.Position])["MALOP"].ToString();
        }


        private void btnSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                this.sINHVIENTableAdapter.Connection.ConnectionString = Program.connectionString;
                this.sINHVIENTableAdapter.Update(this.qLDSVDataSetMain.SINHVIEN);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi lưu sinh viên.\n" + ex.Message, "", MessageBoxButtons.OK);
                return;
            }
            this.lOPGridControl.Enabled = this.comboBoxKhoa.Enabled = btnDelete.Enabled = btnEdit.Enabled =
                sINHVIENGridControl.Enabled = btnInsert.Enabled = btnRefresh.Enabled = true;
            panelTTSV.Enabled = edit = btnSave.Enabled = btnUndo.Enabled = false;
        }

        private void btnDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(nGHIHOCCheckEdit.Checked)
            {
                MessageBox.Show("Bạn đã xóa sinh viên này rồi!");
                return;
            }
            if (MessageBox.Show("Bạn có thật sự muốn xóa sinh viên này ?? ", "Xác nhận",
                       MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                nGHIHOCCheckEdit.Checked = true;
                this.sINHVIENBindingSource.EndEdit();
                this.sINHVIENBindingSource.ResetCurrentItem();
                this.lOPGridControl.Enabled = this.comboBoxKhoa.Enabled = btnEdit.Enabled =
                     btnInsert.Enabled = btnRefresh.Enabled = btnUndo.Enabled = false;
                btnSave.Enabled =  true;
            }
        }

        private void btnEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btnDelete.Enabled = btnEdit.Enabled = btnInsert.Enabled = btnRefresh.Enabled = btnSave.Enabled =
                lOPGridControl.Enabled = sINHVIENGridControl.Enabled = comboBoxKhoa.Enabled = btnHoanTat.Enabled = false;
            panelTTSV.Enabled = grBoxTTSV.Enabled = btnUndo.Enabled = edit = btnOK.Enabled = true;
        }

        private void btnUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.sINHVIENBindingSource.CancelEdit();
            this.sINHVIENTableAdapter.Fill(this.qLDSVDataSetMain.SINHVIEN);
            btnRefresh.Enabled = btnEdit.Enabled = btnInsert.Enabled = btnDelete.Enabled = btnOK.Enabled = btnHoanTat.Enabled =
               this.lOPGridControl.Enabled = this.sINHVIENGridControl.Enabled = this.comboBoxKhoa.Enabled = true;
            panelTTSV.Enabled = btnUndo.Enabled = btnSave.Enabled = false;
            if (okInsert)
            {
                this.lOPGridControl.Enabled = this.sINHVIENGridControl.Enabled = btnRefresh.Enabled = 
                    btnEdit.Enabled = btnInsert.Enabled = btnDelete.Enabled = this.comboBoxKhoa.Enabled = false;
                panelTTSV.Enabled = grBoxTTSV.Enabled = true;
            }
        }

        private void btnRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
                this.sINHVIENTableAdapter.Fill(this.qLDSVDataSetMain.SINHVIEN);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi Refresh :" + ex.Message, "", MessageBoxButtons.OK);
                return;
            }
        }

        private void btnHoanTat_Click(object sender, EventArgs e)
        {
            sINHVIENBindingSource.RemoveAt(sINHVIENBindingSource.Count-1);
            this.sINHVIENBindingSource.ResetCurrentItem();
            btnSave.Enabled =  true;
            panelTTSV.Enabled = btnUndo.Enabled = false;
        }

        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            if (sINHVIENBindingSource.Count == 0 || !khoaKhac)  btnEdit.Enabled= btnDelete.Enabled = false;
            else btnDelete.Enabled = true;
        }

        private void sformSV_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.sformSV = null;
        }

        private void comboBoxKhoa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBoxKhoa.ValueMember != "" && Program.formLogin.Visible == false)
            {
                if (Program.server != this.comboBoxKhoa.SelectedValue.ToString())
                {
                    Program.server = this.comboBoxKhoa.SelectedValue.ToString();
                    if (Program.userNameHTai != Program.remoteLogin)
                    {
                        Program.userNameHTai = Program.remoteLogin;
                        Program.passwordHTai = Program.remotePassword;
                    }
                    else
                    {
                        Program.userNameHTai = Program.userName;
                        Program.passwordHTai = Program.password;
                    }
                    Program.connectionString = "Server = " + Program.server + "; Database = " + Program.database + "; User Id = "
                        + Program.userNameHTai + "; Password = " + Program.passwordHTai;
                    Program.sqlConnection = new SqlConnection(Program.connectionString);
                    Program.sqlConnection.Open();

                    this.dIEMTableAdapter.Connection.ConnectionString = Program.connectionString;
                    this.dIEMTableAdapter.Fill(this.qLDSVDataSetMain.DIEM);

                    this.sINHVIENTableAdapter.Connection.ConnectionString = Program.connectionString;
                    this.sINHVIENTableAdapter.Fill(this.qLDSVDataSetMain.SINHVIEN);

                    this.lOPTableAdapter.Connection.ConnectionString = Program.connectionString;
                    this.lOPTableAdapter.Fill(this.qLDSVDataSetMain.LOP);
                    if (comboBoxKhoa.SelectedValue.ToString() != Program.khoaDN && Program.groupHT == "KHOA")
                    {
                        btnInsert.Enabled = btnSave.Enabled = btnDelete.Enabled = btnEdit.Enabled = khoaKhac = false;
                    }
                    else
                    {
                        btnInsert.Enabled = btnSave.Enabled = btnDelete.Enabled = btnEdit.Enabled = khoaKhac = true;
                    }
                }
            }
        }

        private void btnExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }
    }
}