﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using System.Data.SqlClient;
using QLDSV_CSDLPT.BaoCao;

namespace QLDSV_CSDLPT
{
    static class Program
    {
        public static formMain formMain;
        public static LoginForm formLogin;
        public static formMH formMH;
        public static formLOP formLOP;
        public static sformSV sformSV;
        public static FormDIEM formDiem;
        public static formHP formHocPhi;
        public static BaoCao.XtraForm1 inDanhSachSinhVien;
        public static BaoCao.rpFormDSThiHetMon dSThiHetMon;
        public static BaoCao.rpFormBangDiemMH rpFormBDMH;
        public static BaoCao.rpFormPhieuDiemSV rpFormPDSV;
        public static BaoCao.rpFormDSDongHP rpFormDSHP;

        public static string server = "";
        public static string database = "QLDSV";
        public static string userName = "";
        public static string password = "";
        public static string groupHT = "";

        public static string userNameHTai = "";
        public static string passwordHTai = "";
        public static string remoteLogin = "HTKN";
        public static string remotePassword = "sa";

        public static string khoaDN = "";
        public static BindingSource bindingSourceKhoa;
        public static SqlConnection sqlConnection;
        public static string connectionString = "";



        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            BonusSkins.Register();
            SkinManager.EnableFormSkins();

            formLogin = new LoginForm();

            Application.Run(formLogin);
            //Application.Run(new Form1());
        }
    }
}
