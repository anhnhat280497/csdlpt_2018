﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using System.Data.SqlClient;

namespace QLDSV_CSDLPT
{
    public partial class formHP : DevExpress.XtraEditors.XtraForm
    {
        Boolean edit = false, okInsert = false;
        string nienKhoa; int hocKy;
        int dem = 0;

        public formHP()
        {
            InitializeComponent();
        }


        private void btnFill_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConnection = new SqlConnection(Program.connectionString);
            sqlConnection.Open();
            string sql = "EXEC SP_SVDONGHOCPHI '" + sINHVIENComboBox.Text + "'";

            try
            {
                SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection);
                SqlDataReader dataReader = sqlCommand.ExecuteReader();
                dataReader.Read();
                hOTENLabel1.Text = dataReader.GetString(0);
                mALOPLabel1.Text = dataReader.GetString(1);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Không có sinh viên này!");
                return;
            }
            this.sP_DONGHOCPHITableAdapter.Fill(this.qLDSVDataSetMain.SP_DONGHOCPHI, sINHVIENComboBox.Text);
            bar2.Visible = true;
        }

        private void formHP_Load(object sender, EventArgs e)
        {
            this.sP_DONGHOCPHITableAdapter.Connection.ConnectionString = Program.connectionString;
            this.sINHVIENTableAdapter.Connection.ConnectionString = Program.connectionString;
            // TODO: This line of code loads data into the 'qLDSVDataSetMain.SINHVIEN' table. You can move, or remove it, as needed.
            this.sINHVIENTableAdapter.Fill(this.qLDSVDataSetMain.SINHVIEN);
            bar2.Visible = false;
        }

        private void btnExit_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }

        private void btnEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            nienKhoa = nIENKHOATextEdit.Text; hocKy = Int16.Parse(hOCKYSpinEdit.Text); // giữ lại để làm khóa cho update,
            btnDelete.Enabled = btnEdit.Enabled = btnInsert.Enabled = btnRefresh.Enabled = btnSave.Enabled =
                sP_DONGHOCPHIGridControl.Enabled = btnFill.Enabled = btnHoanTat.Enabled = false;
            panel2.Enabled = btnUndo.Enabled = edit = true;
        }

        private void btnDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            //if (MessageBox.Show("Bạn có thật sự muốn xóa dòng này ?? ", "Xác nhận",
            //           MessageBoxButtons.OKCancel) == DialogResult.OK)
            //{
            //    try
            //    {
            //        nienKhoa = nIENKHOATextEdit.Text; // giữ lại để khi xóa bij lỗi thì ta sẽ quay về lại
            //        hocKy = Int16.Parse(hOCKYSpinEdit.Text); // giữ lại để khi xóa bij lỗi thì ta sẽ quay về lại

            //        sP_DONGHOCPHIBindingSource.RemoveCurrent();
            //        this.sP_DONGHOCPHITableAdapter.Connection.ConnectionString = Program.connectionString;
            //        deleteDL();
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show("Lỗi xóa học phí. Bạn hãy xóa lại\n" + ex.Message, "",
            //            MessageBoxButtons.OK);
            //        this.sP_DONGHOCPHITableAdapter.Fill(this.qLDSVDataSetMain.SP_DONGHOCPHI, sINHVIENComboBox.Text);
            //        sP_DONGHOCPHIBindingSource.Position = sP_DONGHOCPHIBindingSource.Find("NIENKHOA", nienKhoa);
            //        return;
            //    }
            //}

            //if (sP_DONGHOCPHIBindingSource.Count == 0) btnDelete.Enabled = false;
        }

        private void deleteDL()
        {
            string sql = "DELETE FROM dbo.HOCPHI WHERE MASV=@MASV and NIENKHOA=@NIENKHOA and HOCKY=@HOCKY";
            using (SqlConnection connection = new SqlConnection(Program.connectionString))
            using (SqlCommand cm = new SqlCommand(sql, connection))
            {
                cm.Parameters.AddWithValue("@MASV", sINHVIENComboBox.Text);
                cm.Parameters.AddWithValue("@NIENKHOA", nIENKHOATextEdit.Text);
                cm.Parameters.AddWithValue("@HOCKY", Int16.Parse(hOCKYSpinEdit.Text));

                connection.Open();
                try
                {
                    cm.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Lỗi xóa học phí!");
                    connection.Close();
                    return;
                }
                connection.Close();
            }
        }


        private void btnSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.sP_DONGHOCPHIBindingSource.EndEdit();
            if (!edit)
            {
                for (int i = dem; i < sP_DONGHOCPHIBindingSource.Count; i++)
                {
                    string sql = "INSERT INTO dbo.HOCPHI (MASV,NIENKHOA,HOCKY,HOCPHI,SOTIENDADONG) VALUES (@MASV,@NIENKHOA,@HOCKY,@HOCPHI,@SOTIENDADONG)";
                    using (SqlConnection connection = new SqlConnection(Program.connectionString))
                    using (SqlCommand cm = new SqlCommand(sql, connection))
                    {
                        cm.Parameters.AddWithValue("@NIENKHOA", gridView1.GetRowCellValue(i, "NIENKHOA").ToString().Trim());
                        cm.Parameters.AddWithValue("@HOCKY", Int16.Parse(gridView1.GetRowCellValue(i, "HOCKY").ToString().Trim()));
                        cm.Parameters.AddWithValue("@HOCPHI", Int32.Parse(gridView1.GetRowCellValue(i, "HOCPHI").ToString().Trim()));
                        cm.Parameters.AddWithValue("@SOTIENDADONG", Int32.Parse(gridView1.GetRowCellValue(i, "SOTIENDADONG").ToString().Trim()));
                        cm.Parameters.AddWithValue("@MASV", sINHVIENComboBox.Text);
                        connection.Open();
                        try
                        {
                            cm.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Lỗi lưu học phí!");
                            connection.Close();
                            return;
                        }
                        connection.Close();
                    }
                }
            }
            else
            {
                string sql = "UPDATE dbo.HOCPHI SET NIENKHOA=@NIENKHOA,HOCKY=@HOCKY,HOCPHI=@HOCPHI,SOTIENDADONG=@SOTIENDADONG WHERE MASV=@MASV and NIENKHOA=N'" + nienKhoa + "' and HOCKY=" + hocKy;
                using (SqlConnection connection = new SqlConnection(Program.connectionString))
                using (SqlCommand cm = new SqlCommand(sql, connection))
                {
                    cm.Parameters.AddWithValue("@MASV", sINHVIENComboBox.Text);
                    cm.Parameters.AddWithValue("@NIENKHOA", nIENKHOATextEdit.Text);
                    cm.Parameters.AddWithValue("@HOCKY", Int16.Parse(hOCKYSpinEdit.Text));
                    cm.Parameters.AddWithValue("@HOCPHI", Int32.Parse(hOCPHITextEdit.Text));
                    cm.Parameters.AddWithValue("@SOTIENDADONG", Int32.Parse(sOTIENDADONGTextEdit.Text));
                    connection.Open();
                    try
                    {
                        cm.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Lỗi lưu học phí!" + ex.Message);
                        connection.Close();
                        return;
                    }
                    connection.Close();
                }
            }
            this.sP_DONGHOCPHIGridControl.Enabled = btnFill.Enabled = btnDelete.Enabled = btnEdit.Enabled =
                btnInsert.Enabled = btnRefresh.Enabled = true;
            btnSave.Enabled = btnUndo.Enabled = panel2.Enabled = edit = false;
        }

        private void btnUndo_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.sP_DONGHOCPHIBindingSource.CancelEdit();
            this.sP_DONGHOCPHITableAdapter.Fill(this.qLDSVDataSetMain.SP_DONGHOCPHI, sINHVIENComboBox.Text);

            btnRefresh.Enabled = btnEdit.Enabled = btnInsert.Enabled = btnDelete.Enabled =
                this.sP_DONGHOCPHIGridControl.Enabled = btnFill.Enabled = true;
            panel2.Enabled = btnSave.Enabled = btnUndo.Enabled = false;
            if (okInsert)
            {
                btnRefresh.Enabled = btnEdit.Enabled = btnInsert.Enabled = btnDelete.Enabled =
                    this.sP_DONGHOCPHIGridControl.Enabled = btnFill.Enabled = false;
                panel2.Enabled = true;
            }
        }

        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                this.sP_DONGHOCPHITableAdapter.Fill(this.qLDSVDataSetMain.SP_DONGHOCPHI, sINHVIENComboBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi Refresh :" + ex.Message, "", MessageBoxButtons.OK);
                return;
            }
        }

        private void btnInsert_ItemClick(object sender, ItemClickEventArgs e)
        {
            dem = sP_DONGHOCPHIBindingSource.Count;
            this.sP_DONGHOCPHIBindingSource.AddNew();
            this.sP_DONGHOCPHIGridControl.Enabled = btnFill.Enabled = btnDelete.Enabled = btnEdit.Enabled =
                btnInsert.Enabled = btnRefresh.Enabled = btnSave.Enabled = edit = btnHoanTat.Enabled = false;
            panel2.Enabled = btnUndo.Enabled = true;
        }

        private void btnHoanTat_Click(object sender, EventArgs e)
        {
            this.sP_DONGHOCPHIBindingSource.RemoveAt(sP_DONGHOCPHIBindingSource.Count - 1);
            this.sP_DONGHOCPHIBindingSource.ResetCurrentItem();
            btnSave.Enabled = true;
            panel2.Enabled = btnUndo.Enabled = false;
        }

        private void formHP_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.formHocPhi = null;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (nIENKHOATextEdit.Text.Trim() == "")
            {
                MessageBox.Show("Niên khóa không được trống!", "", MessageBoxButtons.OK);
                nIENKHOATextEdit.Focus();
                return;
            }
            if (hOCKYSpinEdit.Text.Trim() == "")
            {
                MessageBox.Show("Học kỳ không được trống!", "", MessageBoxButtons.OK);
                hOCKYSpinEdit.Focus();
                return;
            }
            if (hOCPHITextEdit.Text.Trim() == "")
            {
                MessageBox.Show("Học phí không được trống!", "", MessageBoxButtons.OK);
                hOCPHITextEdit.Focus();
                return;
            }
            if (sOTIENDADONGTextEdit.Text.Trim() == "")
            {
                MessageBox.Show("Số tiền đã đóng không được trống!", "", MessageBoxButtons.OK);
                sOTIENDADONGTextEdit.Focus();
                return;
            }
            int hocPhi = Int32.Parse(hOCPHITextEdit.Text), soTienDong = Int32.Parse(hOCPHITextEdit.Text);
            if (hocPhi < 0)
            {
                MessageBox.Show("Học phí không được âm!");
                hOCPHITextEdit.Focus();
                return;
            }
            if (soTienDong < 0)
            {
                MessageBox.Show("Số tiền đóng không được âm!");
                sOTIENDADONGTextEdit.Focus();
                return;
            }
            string nK = nIENKHOATextEdit.Text, hK = hOCKYSpinEdit.Text;
            int demtrung = 0;
            for (int i = 0; i < sP_DONGHOCPHIBindingSource.Count; i++)
            {
                if (nK == ((DataRowView)sP_DONGHOCPHIBindingSource[i])["NIENKHOA"].ToString().Trim()
                    && hK == ((DataRowView)sP_DONGHOCPHIBindingSource[i])["HOCKY"].ToString().Trim())
                {
                    demtrung++;
                }
            }
            if (demtrung == 2)
            {
                MessageBox.Show("Học kỳ này sinh viên đã đóng tiền!");
                hOCKYSpinEdit.Focus();
                return;
            }
            btnUndo.Enabled = false;
            if (!edit)
            {
                this.sP_DONGHOCPHIBindingSource.AddNew();
                okInsert = btnHoanTat.Enabled = true;
            }
            else
            {
                btnSave.Enabled = true;
                panel2.Enabled = false;
            }
            this.sP_DONGHOCPHIBindingSource.EndEdit();
            this.sP_DONGHOCPHIBindingSource.ResetCurrentItem();
        }


    }
}